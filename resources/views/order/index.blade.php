@extends('layouts.index')
@section('content')
<div class="m-content">
	<div class="m-portlet m-portlet--head-sm m-portlet--collapse" m-portlet="true">
		<div class="m-portlet__head">
			<div class="m-portlet__head-caption">
				<div class="m-portlet__head-title">
					<span class="m-portlet__head-icon">
						<i class="flaticon-search"></i>
					</span>
					<h3 class="m-portlet__head-text">
					Search Tools
					</h3>
				</div>
			</div>
			<div class="m-portlet__head-tools">
				<ul class="m-portlet__nav">
					<li class="m-portlet__nav-item">
						<a href=""  m-portlet-tool="toggle" class="m-portlet__nav-link m-portlet__nav-link--icon">
							<i class="la la-angle-down"></i>
						</a>
					</li>
				</ul>
			</div>
		</div>
		<!--begin::Form-->
		<form action="{{ url()->current() }}" class="m-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed" id="m_form_1" style="display: none; overflow: hidden;">
			<div class="m-portlet__body">
				<div class="form-group m-form__group row">
					<div class="col-lg-4">
						<label>
							Serial Number:
						</label>
						<input type="text" id="input_sn" class="form-control m-input" placeholder="Serial Number" name="sn" onkeyup="submitInput(event)">
						<span class="m-form__help"></span>
					</div>
					<div class="col-lg-4">
						<label class="">
							Brand Name:
						</label>
						<select class="form-control m-select2" id="m_select2_1" name="brand_id" onchange="submitSelect(this)">
							<option value="off" selected>Select</option>
							@foreach($brand as $key => $value)
								<option value="{{ $key }}">{{ $value }}</option>
							@endforeach
						</select>
						<span class="m-form__help"></span>
					</div>
					<div class="col-lg-4">
						<label>
							Model Name:
						</label>
						<select class="form-control m-select2" id="m_select2_2" id="input_model_name" name="model_id" onchange="submitSelect(this)">
							<option value="off" selected>Select</option>
							@foreach($model as $key => $value)
								<option value="{{ $key }}">{{ $value }}</option>
							@endforeach
						</select>
						<span class="m-form__help"></span>
					</div>
					<div class="col-lg-4">
						<label>
							Grade:
						</label>
						<select class="form-control m-select2" id="m_select2_3" id="input_grade_name" name="grade_id" onchange="submitSelect(this)">
							<option value="off" selected>Select</option>
							@foreach($grade as $key => $value)
								<option value="{{ $key }}">{{ $value }}</option>
							@endforeach
						</select>
						<span class="m-form__help"></span>
					</div>
					<div class="col-lg-4">
						<label class="">
							Sku Name:
						</label>
						<input type="text" class="form-control m-input" id="input_sku_name" name="sku_id" placeholder="Example 32G Or 2G" onkeyup="submitInput(event)">
						<span class="m-form__help"></span>
					</div>
				</div>
				<div class="form-group m-form__group row">
					<div class="col-lg-12">
						<label class="">
							Range Price
						</label>
						<div class="row align-items-center">
							<div class="col-sm-4 col-md-3 col-lg-2">
								<input type="text" class="form-control" id="m_nouislider_3_input" name="start_price" placeholder="Start Price" onchange="submitInput(event)">
								<span class="m-form__help"></span>
							</div>
							<div class="col-sm-4 col-md-3 col-lg-2">
								<input type="text" class="form-control" id="m_nouislider_3.1_input" name="end_price" placeholder="End Price" onkeyup="submitInput(event)">
								<span class="m-form__help"></span>
							</div>
							<div class="col-lg-8">
								<div id="m_nouislider_3" class="m-nouislider"></div>
								<span class="m-form__help"></span>
							</div>
						</div>
					</div>
				</div>
				<div class="form-group m-form__group row">
						<div class="col-lg-12">
							<label class="">
								Difference Price
							</label>
							<div class="col-sm-12 col-md-12 col-lg-12">
								<input type="hidden" id="m_slider_1" name="diff_price" onchange="submitSlide(event)">
								<span class="m-form__help"></span>
							</div>
						</div>
					</div>
			</div>
			<div class="m-portlet__foot m-portlet__foot--fit">
				<div class="m-form__actions">
					<button id="submit" class="btn btn-accent">
					Submit
					</button>
					<a href="{{ route('order.index') }}" class="btn btn-secondary">
						Reset
					</a>
				</div>
			</div>
		</form>
		<!--end::Form-->
	</div>
	<div class="m-portlet m-portlet--mobile">
		<div class="m-portlet__head">
			<div class="m-portlet__head-caption">
				<div class="m-portlet__head-title">
					<h3 class="m-portlet__head-text">
					Phone List
					</h3>
				</div>
			</div>
		</div>
		<div class="m-portlet__body">
			<!--begin: Datatable -->
			<table class="m-datatable" id="html_table" width="100%">
				<thead>
					<tr>
						<th data-field="no">
							No
						</th>
						<th data-field="sn">
							Serial Number
						</th>
						<th>
							Brand
						</th>
						<th>
							Name
						</th>
						<th>
							Sku Name
						</th>
						<th>
							Price
						</th>
						<th>
							Grade
						</th>
						<th>
							Model Picture
						</th>
						<th>
							Status
						</th>
						<th>
							Date
						</th>
						<th>
							Action
						</th>
					</tr>
				</thead>
				<tbody>
					@foreach ($orders as $order)
					<tr>
						<td>
							{{ ++$i }}
						</td>
						<td>
							{{ $order->fsn }}
						</td>
						<td>
							{{ $order->preorder->itembase->brand->fname }}
						</td>
						<td>
							{{ $order->fproduct_name }}
						</td>
						<td>
							{{ $order->preorder->fsku_name }}
						</td>
						<td>
							{{ number_format($order->fdeal_amount, 0) }}
						</td>
						<td>
							{{ ($order->preorder->grade) ? $order->preorder->grade->fname : '' }} - {{ ($order->qcBill) ? $order->qcBill->fqcGradeName : '' }}
						</td>
						<td>
							@foreach($order->orderfile->where('ftype', 2) as $orderfile)
							<a href="{{ 'https://ossapi.joyexchange.id/'.$orderfile->fpath }}" data-fancybox="images" data-caption="Serial Number : {{ $order->fsn }}&lt;/b&gt;&lt;br /&gt;Name : {{ $order->fproduct_name }}&lt;/b&gt;&lt;br /&gt;Sku Name : {{ $order->preorder->fsku_name }}&lt;/b&gt;&lt;br /&gt;Price : {{ number_format($order->fdeal_amount, 0) }}&lt;/b&gt;&lt;br /&gt;Grade : {{ ($order->qcBill) ? $order->qcBill->fqcGradeName: isset($order->preorder->grade->fname) }}">
								<img src="{{ 'https://ossapi.joyexchange.id/'.$orderfile->fpath }}" width="50" height="50">
							</a>
							@endforeach
						</td>
						<td>
							{{ $order->statusName[$order->fstatus] }}
						</td>
						<td>
							{{ date('Y-m-d', $order->fbill_time) }}
						</td>
						<td>
							<a onclick="view(this)" data-id="{{ $order->fsn }}" data-toggle="modal" data-target="#m_modal_2" class="m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill" title="View">
								<i class="la la-eye"></i>
							</a>
							<a href="{{ route('order.similar', [
								'order_id' => $order->fid,
								'brand_id' => $order->preorder->itembase->brand->fid,
								'model_id' => $order->preorder->itembase->fid,
								'sku_id' => $order->preorder->fitemsku_id,
								'grade_id' => $order->qcBill ? $order->qcBill->fqcGradeId : $order->preorder->fgrade_id,
								'status_id' => $order->fstatus,
								]) }}" 
								target="_blank" class="m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill" title="Look for something similar">
								<i class="la la-search"></i>
							</a>
							<a onclick="cart(this)" data-id="{{ $order->fid }}" data-sn="{{ $order->fsn }}" class="m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill" title="Add To Chart">
								<i class="la la-plus-circle"></i>
							</a>
						</td>
					</tr>
					@endforeach
				</tbody>
			</table>
			{{ $orders->links() }}
			<!--end: Datatable -->
		</div>
	</div>
</div>
<div class="m-content">
	<div class="modal fade" id="m_modal_2" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title" id="exampleModalLongTitle">
					Modal title
					</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">
						&times;
					</span>
					</button>
				</div>
				<div class="modal-body">
					<ul class="nav nav-pills nav-fill" role="tablist">
						<li class="nav-item">
							<a class="nav-link active" data-toggle="tab" href="#m_tabs_5_1">
								Basic Information
							</a>
						</li>
						<li class="nav-item">
							<a class="nav-link" data-toggle="tab" href="#m_tabs_5_2">
								Inspection report
							</a>
						</li>
						<li class="nav-item">
							<a class="nav-link" data-toggle="tab" href="#m_tabs_5_3">
								image information
							</a>
						</li>
					</ul>
					<div class="tab-content">
						<div class="tab-pane active" id="m_tabs_5_1" role="tabpanel">
							<!--begin::Portlet-->
							<div class="m-portlet" style="margin-bottom: 0;">
								<div class="m-portlet__body">
									<!--begin::Section-->
									<div class="m-section" style="margin-bottom: 0;">
										<div class="m-section__content">
											<div class="table-responsive">
												<table class="table table-bordered m-table m-table--border-success">
													<thead>
														<tr>
															<td>
																Serial Number : <td id="serial_number"></td>
															</td>
														</tr>
														<tr>
															<td>
																Brand Name : <td id="brand_name"></td>
															</td>
														</tr>
														<tr>
															<td>
																Model Name : <td id="model_name"></td>
															</td>
														</tr>
														<tr>
															<td>
																Grade Name : <td id="grade_name"></td>
															</td>
														</tr>
														<tr>
															<td>
																Price : <td id="price"></td>
															</td>
														</tr>
														<tr>
															<td>
																Sku Name : <td id="sku_name"></td>
															</td>
														</tr>
													</thead>
												</table>
											</div>
										</div>
									</div>
									<!--end::Section-->
								</div>
								<!--end::Form-->
							</div>
							<!--end::Portlet-->
						</div>
						
						<div class="tab-pane" id="m_tabs_5_2" role="tabpanel">
							<!--begin::Portlet-->
							<div class="m-portlet">
								<div class="m-portlet__body">
									<!--begin::Section-->
									<div class="m-section">
										<div class="m-section__content">
											<div class="table-responsive">
												<table class="table table-bordered m-table m-table--border-success">
													<thead>
														<tr>
															<td>
																Property
															</td>
															<td>
																Store quality
															</td>
															<td>
																QC quality
															</td>
														</tr>
													</thead>
													<tbody id="reportProperty">
													</tbody>
												</table>
											</div>
										</div>
									</div>
									<!--end::Section-->
								</div>
								<!--end::Form-->
							</div>
							<!--end::Portlet-->
						</div>
						<div class="tab-pane" id="m_tabs_5_3" role="tabpanel">
							<!--begin::Portlet-->
							<div class="m-portlet">
								<div class="m-portlet__body">
									<!--begin::Section-->
									<div class="m-section">
										<div class="m-section__content">
											<div class="table-responsive">
												<table class="table table-bordered m-table m-table--border-success">
													<thead>
														<tr>
															<td>
																Model Picture
															</td>
															<td>
																Id Card Picture
															</td>
														</tr>
													</thead>
													<tbody>
														<tr>
															<td id="model_picture">
															</td>
															<td id="id_card_picture">
															</td>
														</tr>
													</tbody>
												</table>
											</div>
										</div>
									</div>
									<!--end::Section-->
								</div>
								<!--end::Form-->
							</div>
							<!--end::Portlet-->
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary" data-dismiss="modal">
					Close
					</button>
				</div>
			</div>
		</div>
	</div>
</div>
<script>
function view(number) {
	getElementByIdClear();

	var sn = $(number).data('id');
	
	$.ajaxSetup({
		headers: {
			'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		}
	});
	$.ajax({
		type: 'GET',
		url: '/order/' + sn,
		dataType: 'json',
		success: function(data) {
			document.getElementById("serial_number").innerHTML = data.order.serial_number;
			document.getElementById("brand_name").innerHTML = data.order.brand_name;
			document.getElementById("model_name").innerHTML = data.order.model_name;
			document.getElementById("grade_name").innerHTML = data.order.grade_name;
			document.getElementById("price").innerHTML = data.order.price;
			document.getElementById("sku_name").innerHTML = data.order.sku_name;

			data.order.model_picture.forEach(function(model_picture) {
				modelPicture(model_picture);
			});

			data.order.id_card_picture.forEach(function(id_card_picture) {
				idCardPicture(id_card_picture);
			});

			data.reportProperty.forEach(function(value) {
				if(value.property_image_qc != null) {
					value.property_image_qc.forEach(function(image) {
						image_url = '<br><a href="'+image.image_url+'" data-fancybox="image_url"><img src="'+image.image_url+'" width="50" height="50"></a>';
						document.getElementById("reportProperty").innerHTML += '<tr><td>'+value.property_name_store+'</td><td>'+value.property_value_store+'</td><td>'+value.property_value_qc+image_url+'</td></tr>';
					});					
				} else {
					document.getElementById("reportProperty").innerHTML += '<tr><td>'+value.property_name_store+'</td><td>'+value.property_value_store+'</td><td>'+value.property_value_qc+'</td></tr>';
				}
			});
		},
		error: function(data) {
			console.log(data);
		}
	});
}

function getElementByIdClear() {
	document.getElementById("serial_number").innerHTML = '';
	document.getElementById("brand_name").innerHTML = '';
	document.getElementById("model_name").innerHTML = '';
	document.getElementById("grade_name").innerHTML = '';
	document.getElementById("price").innerHTML = '';
	document.getElementById("sku_name").innerHTML = '';
	document.getElementById("model_picture").innerHTML = '';
	document.getElementById("id_card_picture").innerHTML = '';
	document.getElementById("reportProperty").innerHTML = '';
}

function modelPicture(image) {
	document.getElementById("model_picture").innerHTML += '<a href="'+image.url+'" data-fancybox="model_picture"><img src="'+image.url+'" width="50" height="50" style="padding: 5px;"></a>';
}

function idCardPicture(image) {
	document.getElementById("id_card_picture").innerHTML += '<a href="'+image.url+'" data-fancybox="id_card_picture"><img src="'+image.url+'" width="50" height="50" style="padding: 5px;"></a>';
}

var submit = document.getElementById("submit");
submit.disabled = true;

function submitInput(event) {
	if (event.target.value != "" || event.target.value != 0) {
		submit.disabled = false;
	} else {
		submit.disabled = true;
	}
}

function submitSelect(event) {
	if (event.options[event.selectedIndex].value != "off") {
		submit.disabled = false;
	} else {
		submit.disabled = true;
	}
}

function submitSlide(event) {
	if (event.target.value != 0) {
		submit.disabled = false;
	} else {
		submit.disabled = true;
	}
}

function cart(number) {

	var fid = $(number).data('id');
	var fsn = $(number).data('sn');
	notification();
	
	$.ajaxSetup({
		headers: {
			'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		}
	});
	$.ajax({
		type: 'POST',
		url: '{{ route('order.cart') }}',
		data: {
			'id': fid,
			'sn': fsn,
		},
		dataType: 'json',
		success: function(data,status,xhr) {
			if (xhr.status === 200) {
				toastr.success('phone success add cart');				
			}
		},
		error :function(data) {
		   	if( data.status === 422 ) {
		   		toastr.warning('phone has been taken or sold');
		    }
		}
	});
}

function notification() {
	return toastr.options = {
	  "closeButton": true,
	  "debug": false,
	  "newestOnTop": true,
	  "progressBar": false,
	  "positionClass": "toast-top-right",
	  "preventDuplicates": false,
	  "onclick": null,
	  "showDuration": "300",
	  "hideDuration": "1000",
	  "timeOut": "5000",
	  "extendedTimeOut": "1000",
	  "showEasing": "swing",
	  "hideEasing": "linear",
	  "showMethod": "fadeIn",
	  "hideMethod": "fadeOut"
	};
}
</script>
@endsection
