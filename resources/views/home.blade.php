@extends('layouts.index')
@section('content')

<div class="m-content">
    <div class="row">
        <div class="col-lg-12">
            <div class="m-portlet m-portlet--tab">
                <div class="m-portlet__head">
                    <div class="m-portlet__head-caption">
                        <div class="m-portlet__head-title">
                            <span class="m-portlet__head-icon m--hide">
                                <i class="la la-gear"></i>
                            </span>
                            <h3 class="m-portlet__head-text" style="text-transform: uppercase;">Welcome {{ Auth::user()->name }}</h3>
                        </div>
                    </div>
                </div>
                <div class="m-portlet__body">
                    <div id="m_flotcharts_2" style="height: 300px;"></div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection