<!DOCTYPE html>
<!--
Template Name: Metronic - Responsive Admin Dashboard Template build with Twitter Bootstrap 4
Author: KeenThemes
Website: http://www.keenthemes.com/
Contact: support@keenthemes.com
Follow: www.twitter.com/keenthemes
Dribbble: www.dribbble.com/keenthemes
Like: www.facebook.com/keenthemes
Purchase: http://themeforest.net/item/metronic-responsive-admin-dashboard-template/4021469?ref=keenthemes
Renew Support: http://themeforest.net/item/metronic-responsive-admin-dashboard-template/4021469?ref=keenthemes
License: You must have a valid license purchased only from themeforest(the above link) in order to legally use the theme for your project.
-->
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
	<!-- begin::Head -->
	<head>
		<meta charset="utf-8" />
		<title>{{ config('app.name', 'Laravel') }}</title>
		<!-- CSRF Token -->
		<meta name="csrf-token" content="{{ csrf_token() }}">
		<meta name="description" content="Latest updates and statistic charts">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<!--begin::Web font -->
		<script src="https://ajax.googleapis.com/ajax/libs/webfont/1.6.16/webfont.js"></script>
		<script>
		WebFont.load({
		google: {"families":["Poppins:300,400,500,600,700","Roboto:300,400,500,600,700"]},
		active: function() {
		sessionStorage.fonts = true;
		}
		});
		</script>
		<!--end::Web font -->
		<!--begin::Base Styles -->
		<!--begin::Page Vendors -->
		<link href="{{ asset('assets/vendors/custom/fullcalendar/fullcalendar.bundle.css') }}" rel="stylesheet" type="text/css" />
		<!--end::Page Vendors -->
		<link href="{{ asset('assets/vendors/base/vendors.bundle.css') }}" rel="stylesheet" type="text/css" />
		<link href="{{ asset('assets/demo/default/base/style.bundle.css') }}" rel="stylesheet" type="text/css" />
		<!--end::Base Styles -->
		<!-- start::fancyBox -->
		<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.5.7/jquery.fancybox.min.css">
		<!-- end::fancyBox -->
		{{-- start::select2 --}}
		<style>
			.select2 {
				width:100%!important;
			}
		</style>
		{{-- end::select2 --}}

		<link rel="shortcut icon" href="{{ asset('assets/demo/default/media/img/logo/favicon.ico') }}" />

	</head>
	<!-- end::Head -->
	<!-- end::Body -->
	<body  class="m-page--fluid m--skin- m-content--skin-light2 m-header--fixed m-header--fixed-mobile m-aside-left--enabled m-aside-left--skin-dark m-aside-left--offcanvas m-footer--push m-aside--offcanvas-default"  >
		<!-- begin:: Page -->
		<div class="m-grid m-grid--hor m-grid--root m-page">
			<!-- BEGIN: Header -->
			<header id="m_header" class="m-grid__item    m-header "  m-minimize-offset="200" m-minimize-mobile-offset="200" >
				<div class="m-container m-container--fluid m-container--full-height">
					<div class="m-stack m-stack--ver m-stack--desktop">
						<!-- BEGIN: Brand -->
						<div class="m-stack__item m-brand  m-brand--skin-dark ">
							<div class="m-stack m-stack--ver m-stack--general">
								<div class="m-stack__item m-stack__item--middle m-brand__logo">
									<a href="{{ URL::to('/') }}" class="m-brand__logo-wrapper">
										<h3 style="color: white;">{{ config('app.name', 'Laravel') }}</h3>
									</a>
								</div>
								<div class="m-stack__item m-stack__item--middle m-brand__tools">
									<!-- BEGIN: Left Aside Minimize Toggle -->
									<a href="javascript:;" id="m_aside_left_minimize_toggle" class="m-brand__icon m-brand__toggler m-brand__toggler--left m--visible-desktop-inline-block
										">
										<span></span>
									</a>
									<!-- END -->
									<!-- BEGIN: Responsive Aside Left Menu Toggler -->
									<a href="javascript:;" id="m_aside_left_offcanvas_toggle" class="m-brand__icon m-brand__toggler m-brand__toggler--left m--visible-tablet-and-mobile-inline-block">
										<span></span>
									</a>
									<!-- END -->
									<!-- BEGIN: Responsive Header Menu Toggler -->
									<a id="m_aside_header_menu_mobile_toggle" href="javascript:;" class="m-brand__icon m-brand__toggler m--visible-tablet-and-mobile-inline-block">
										<span></span>
									</a>
									<!-- END -->
									<!-- BEGIN: Topbar Toggler -->
									<a id="m_aside_header_topbar_mobile_toggle" href="javascript:;" class="m-brand__icon m--visible-tablet-and-mobile-inline-block">
										<i class="flaticon-more"></i>
									</a>
									<!-- BEGIN: Topbar Toggler -->
								</div>
							</div>
						</div>
						<!-- END: Brand -->
						<div class="m-stack__item m-stack__item--fluid m-header-head" id="m_header_nav">
							<!-- BEGIN: Horizontal Menu -->
							<button class="m-aside-header-menu-mobile-close  m-aside-header-menu-mobile-close--skin-dark " id="m_aside_header_menu_mobile_close_btn">
							<i class="la la-close"></i>
							</button>
							<!-- END: Horizontal Menu -->
							<!-- BEGIN: Topbar -->
							<div id="m_header_topbar" class="m-topbar  m-stack m-stack--ver m-stack--general m-stack--fluid">
								<div class="m-stack__item m-topbar__nav-wrapper">
									<ul class="m-topbar__nav m-nav m-nav--inline">
										<li id="showCart" class="m-nav__item m-topbar__quick-actions m-topbar__quick-actions--img m-dropdown m-dropdown--large m-dropdown--header-bg-fill m-dropdown--arrow m-dropdown--align-right m-dropdown--align-push m-dropdown--mobile-full-width m-dropdown--skin-light" m-dropdown-toggle="click" m-dropdown-persistent="1">
											<a href="#" class="m-nav__link m-dropdown__toggle" id="m_topbar_notification_icon">
												<span class="m-nav__link-badge m-badge m-badge--dot m-badge--dot-small m-badge--danger"></span>
												<span class="m-nav__link-icon">
													<i class="flaticon-cart"></i>
												</span>
											</a>
											<div class="m-dropdown__wrapper">
												<span class="m-dropdown__arrow m-dropdown__arrow--right m-dropdown__arrow--adjust"></span>
												<div class="m-dropdown__inner">
													<div class="m-dropdown__header m--align-center" style="background: url(assets/app/media/img/misc/notification_bg.jpg); background-size: cover;">
														<span id="cart-total"class="m-dropdown__header-title"></span>
														<span id="cart-notifications" class="m-dropdown__header-subtitle"></span>
													</div>
													<div class="m-dropdown__body">
														<div class="m-dropdown__content">
															<ul class="nav nav-tabs m-tabs m-tabs-line m-tabs-line--brand" role="tablist">
																<li class="nav-item m-tabs__item">
																	<a class="nav-link m-tabs__link active" data-toggle="tab" href="#topbar_notifications_notifications" role="tab">
																		Menunggu
																	</a>
																</li>
																<li class="nav-item m-tabs__item">
																	<a class="nav-link m-tabs__link" data-toggle="tab" href="#topbar_notifications_events" role="tab">
																		Diproses
																	</a>
																</li>
																<li class="nav-item m-tabs__item">
																	<a class="nav-link m-tabs__link" data-toggle="tab" href="#topbar_notifications_logs" role="tab">
																		Selesai
																	</a>
																</li>
															</ul>
															<div class="tab-content">
																<div class="tab-pane active" id="topbar_notifications_notifications" role="tabpanel">
																	<div class="m-scrollable" data-scrollable="true" data-max-height="250" data-mobile-max-height="200">
																		<div class="m-list-timeline m-list-timeline--skin-light">
																			<div id="menunggu-keputusan" class="m-list-timeline__items">
																			</div>
																		</div>
																	</div>
																</div>
																<div class="tab-pane" id="topbar_notifications_events" role="tabpanel">
																	<div class="m-scrollable" data-scrollable="true" data-max-height="250" data-mobile-max-height="200">
																		<div class="m-list-timeline m-list-timeline--skin-light">
																			<div class="m-list-timeline__items">
																				<div class="m-list-timeline__item">
																					<span class="m-list-timeline__badge m-list-timeline__badge--state1-success"></span>
																					<a href="" class="m-list-timeline__text">
																						New order received
																					</a>
																					<span class="m-list-timeline__time">
																						Just now
																					</span>
																				</div>
																				<div class="m-list-timeline__item">
																					<span class="m-list-timeline__badge m-list-timeline__badge--state1-danger"></span>
																					<a href="" class="m-list-timeline__text">
																						New invoice received
																					</a>
																					<span class="m-list-timeline__time">
																						20 mins
																					</span>
																				</div>
																				<div class="m-list-timeline__item">
																					<span class="m-list-timeline__badge m-list-timeline__badge--state1-success"></span>
																					<a href="" class="m-list-timeline__text">
																						Production server up
																					</a>
																					<span class="m-list-timeline__time">
																						5 hrs
																					</span>
																				</div>
																				<div class="m-list-timeline__item">
																					<span class="m-list-timeline__badge m-list-timeline__badge--state1-info"></span>
																					<a href="" class="m-list-timeline__text">
																						New order received
																					</a>
																					<span class="m-list-timeline__time">
																						7 hrs
																					</span>
																				</div>
																				<div class="m-list-timeline__item">
																					<span class="m-list-timeline__badge m-list-timeline__badge--state1-info"></span>
																					<a href="" class="m-list-timeline__text">
																						System shutdown
																					</a>
																					<span class="m-list-timeline__time">
																						11 mins
																					</span>
																				</div>
																				<div class="m-list-timeline__item">
																					<span class="m-list-timeline__badge m-list-timeline__badge--state1-info"></span>
																					<a href="" class="m-list-timeline__text">
																						Production server down
																					</a>
																					<span class="m-list-timeline__time">
																						3 hrs
																					</span>
																				</div>
																			</div>
																		</div>
																	</div>
																</div>
																<div class="tab-pane" id="topbar_notifications_logs" role="tabpanel">
																	<div class="m-stack m-stack--ver m-stack--general" style="min-height: 180px;">
																		<div class="m-stack__item m-stack__item--center m-stack__item--middle">
																			<span class="">
																				All caught up!
																				<br>
																				No new logs.
																			</span>
																		</div>
																	</div>
																</div>
															</div>
														</div>
													</div>
												</div>
											</div>
										</li>
										<li class="m-nav__item m-topbar__user-profile m-topbar__user-profile--img  m-dropdown m-dropdown--medium m-dropdown--arrow m-dropdown--header-bg-fill m-dropdown--align-right m-dropdown--mobile-full-width m-dropdown--skin-light" m-dropdown-toggle="click">
											<a href="#" class="m-nav__link m-dropdown__toggle">
												<span class="m-topbar__userpic">
													<img src="{{ asset('assets/app/media/img/users/user4.jpg') }}" class="m--img-rounded m--marginless m--img-centered" alt=""/>
												</span>
												<span class="m-topbar__username m--hide">
													{{ Auth::user()->name }}
												</span>
											</a>
											<div class="m-dropdown__wrapper">
												<span class="m-dropdown__arrow m-dropdown__arrow--right m-dropdown__arrow--adjust"></span>
												<div class="m-dropdown__inner">
													<div class="m-dropdown__header m--align-center" style="background: url({{ ('assets/app/media/img/misc/user_profile_bg.jpg') }}); background-size: cover;">
														<div class="m-card-user m-card-user--skin-dark">
															<div class="m-card-user__pic">
																<img src="{{ asset('assets/app/media/img/users/user4.jpg') }}" class="m--img-rounded m--marginless" alt=""/>
															</div>
															<div class="m-card-user__details" style="display: flex; flex-wrap: nowrap;">
																<span class="m-card-user__name m--font-weight-500">
																	{{ Auth::user()->name }}
																</span>
															</div>
														</div>
													</div>
													<div class="m-dropdown__body">
														<div class="m-dropdown__content">
															<ul class="m-nav m-nav--skin-light">
																<li class="m-nav__section m--hide">
																	<span class="m-nav__section-text">
																		Section
																	</span>
																</li>
																<li class="m-nav__separator m-nav__separator--fit"></li>
																<li class="m-nav__item">
																	<a href="{{ route('logout') }}" class="btn m-btn--pill btn-secondary m-btn m-btn--custom m-btn--label-brand m-btn--bolder" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
																		Logout
																	</a>
																	<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
																	    @csrf
																	</form>
																</li>
															</ul>
														</div>
													</div>
												</div>
											</div>
										</li>
									</ul>
								</div>
							</div>
							<!-- END: Topbar -->
						</div>
					</div>
				</div>
			</header>
			<!-- END: Header -->
			<!-- begin::Body -->
			<div class="m-grid__item m-grid__item--fluid m-grid m-grid--ver-desktop m-grid--desktop m-body">
				<!-- BEGIN: Left Aside -->
				<button class="m-aside-left-close  m-aside-left-close--skin-dark " id="m_aside_left_close_btn">
				<i class="la la-close"></i>
				</button>
				<div id="m_aside_left" class="m-grid__item	m-aside-left  m-aside-left--skin-dark ">
					<!-- BEGIN: Aside Menu -->
					<div
						id="m_ver_menu"
						class="m-aside-menu  m-aside-menu--skin-dark m-aside-menu--submenu-skin-dark "
						m-menu-vertical="1"
						m-menu-scrollable="0" m-menu-dropdown-timeout="500"
						>
						<ul class="m-menu__nav  m-menu__nav--dropdown-submenu-arrow ">
							<li class="m-menu__item  m-menu__item--active" aria-haspopup="true" >
								<a  href="{{ url('/') }}" class="m-menu__link ">
									<i class="m-menu__link-icon flaticon-line-graph"></i>
									<span class="m-menu__link-title">
										<span class="m-menu__link-wrap">
											<span class="m-menu__link-text">
												Dashboard
											</span>
											<span class="m-menu__link-badge">
												<span class="m-badge m-badge--danger">
													2
												</span>
											</span>
										</span>
									</span>
								</a>
							</li>
							<li class="m-menu__section ">
								<h4 class="m-menu__section-text">
								Components
								</h4>
								<i class="m-menu__section-icon flaticon-more-v3"></i>
							</li>
							<li class="m-menu__item  m-menu__item--submenu" aria-haspopup="true"  m-menu-submenu-toggle="hover">
								<a  href="{{ route('home.chart') }}" class="m-menu__link m-menu__toggle">
									<i class="m-menu__link-icon flaticon-graphic-1"></i>
									<span class="m-menu__link-text">
										Preorder
									</span>
								</a>
							</li>
							<li class="m-menu__item  m-menu__item--submenu" aria-haspopup="true"  m-menu-submenu-toggle="hover">
								<a  href="{{ route('order.index') }}" class="m-menu__link m-menu__toggle">
									<i class="m-menu__link-icon flaticon-clock"></i>
									<span class="m-menu__link-text">
										Order
									</span>
								</a>
							</li>
							<li class="m-menu__item  m-menu__item--submenu" aria-haspopup="true"  m-menu-submenu-toggle="hover">
								<a  href="{{ route('warehouse.index') }}" class="m-menu__link m-menu__toggle">
									<i class="m-menu__link-icon flaticon-graphic-1"></i>
									<span class="m-menu__link-text">
										Warehouse
									</span>
								</a>
							</li>
							<li class="m-menu__item  m-menu__item--submenu" aria-haspopup="true"  m-menu-submenu-toggle="hover">
								<a  href="javascript:;" class="m-menu__link m-menu__toggle">
									<i class="m-menu__link-icon flaticon-share"></i>
									<span class="m-menu__link-text">
										Icons
									</span>
									<i class="m-menu__ver-arrow la la-angle-right"></i>
								</a>
								<div class="m-menu__submenu ">
									<span class="m-menu__arrow"></span>
									<ul class="m-menu__subnav">
										<li class="m-menu__item " aria-haspopup="true" >
											<a  href="components/icons/flaticon.html" class="m-menu__link ">
												<i class="m-menu__link-bullet m-menu__link-bullet--dot">
												<span></span>
												</i>
												<span class="m-menu__link-text">
													Flaticon
												</span>
											</a>
										</li>
										<li class="m-menu__item " aria-haspopup="true" >
											<a  href="components/icons/fontawesome.html" class="m-menu__link ">
												<i class="m-menu__link-bullet m-menu__link-bullet--dot">
												<span></span>
												</i>
												<span class="m-menu__link-text">
													Fontawesome
												</span>
											</a>
										</li>
										<li class="m-menu__item " aria-haspopup="true" >
											<a  href="components/icons/lineawesome.html" class="m-menu__link ">
												<i class="m-menu__link-bullet m-menu__link-bullet--dot">
												<span></span>
												</i>
												<span class="m-menu__link-text">
													Lineawesome
												</span>
											</a>
										</li>
										<li class="m-menu__item " aria-haspopup="true" >
											<a  href="components/icons/socicons.html" class="m-menu__link ">
												<i class="m-menu__link-bullet m-menu__link-bullet--dot">
												<span></span>
												</i>
												<span class="m-menu__link-text">
													Socicons
												</span>
											</a>
										</li>
									</ul>
								</div>
							</li>
							<li class="m-menu__item  m-menu__item--submenu" aria-haspopup="true"  m-menu-submenu-toggle="hover">
								<a  href="javascript:;" class="m-menu__link m-menu__toggle">
									<i class="m-menu__link-icon flaticon-multimedia-1"></i>
									<span class="m-menu__link-text">
										Buttons
									</span>
									<i class="m-menu__ver-arrow la la-angle-right"></i>
								</a>
								<div class="m-menu__submenu ">
									<span class="m-menu__arrow"></span>
									<ul class="m-menu__subnav">
										<li class="m-menu__item  m-menu__item--parent" aria-haspopup="true" >
											<span class="m-menu__link">
												<span class="m-menu__link-text">
													Buttons
												</span>
											</span>
										</li>
										<li class="m-menu__item  m-menu__item--submenu" aria-haspopup="true"  m-menu-submenu-toggle="hover">
											<a  href="javascript:;" class="m-menu__link m-menu__toggle">
												<i class="m-menu__link-bullet m-menu__link-bullet--dot">
												<span></span>
												</i>
												<span class="m-menu__link-text">
													Button Base
												</span>
												<i class="m-menu__ver-arrow la la-angle-right"></i>
											</a>
											<div class="m-menu__submenu ">
												<span class="m-menu__arrow"></span>
												<ul class="m-menu__subnav">
													<li class="m-menu__item " aria-haspopup="true" >
														<a  href="components/buttons/base/default.html" class="m-menu__link ">
															<i class="m-menu__link-bullet m-menu__link-bullet--dot">
															<span></span>
															</i>
															<span class="m-menu__link-text">
																Default Style
															</span>
														</a>
													</li>
													<li class="m-menu__item " aria-haspopup="true" >
														<a  href="components/buttons/base/square.html" class="m-menu__link ">
															<i class="m-menu__link-bullet m-menu__link-bullet--dot">
															<span></span>
															</i>
															<span class="m-menu__link-text">
																Square Style
															</span>
														</a>
													</li>
													<li class="m-menu__item " aria-haspopup="true" >
														<a  href="components/buttons/base/pill.html" class="m-menu__link ">
															<i class="m-menu__link-bullet m-menu__link-bullet--dot">
															<span></span>
															</i>
															<span class="m-menu__link-text">
																Pill Style
															</span>
														</a>
													</li>
													<li class="m-menu__item " aria-haspopup="true" >
														<a  href="components/buttons/base/air.html" class="m-menu__link ">
															<i class="m-menu__link-bullet m-menu__link-bullet--dot">
															<span></span>
															</i>
															<span class="m-menu__link-text">
																Air Style
															</span>
														</a>
													</li>
												</ul>
											</div>
										</li>
										<li class="m-menu__item " aria-haspopup="true" >
											<a  href="components/buttons/group.html" class="m-menu__link ">
												<i class="m-menu__link-bullet m-menu__link-bullet--dot">
												<span></span>
												</i>
												<span class="m-menu__link-text">
													Button Group
												</span>
											</a>
										</li>
										<li class="m-menu__item " aria-haspopup="true" >
											<a  href="components/buttons/dropdown.html" class="m-menu__link ">
												<i class="m-menu__link-bullet m-menu__link-bullet--dot">
												<span></span>
												</i>
												<span class="m-menu__link-text">
													Button Dropdown
												</span>
											</a>
										</li>
										<li class="m-menu__item  m-menu__item--submenu" aria-haspopup="true"  m-menu-submenu-toggle="hover">
											<a  href="javascript:;" class="m-menu__link m-menu__toggle">
												<i class="m-menu__link-bullet m-menu__link-bullet--dot">
												<span></span>
												</i>
												<span class="m-menu__link-text">
													Button Icon
												</span>
												<i class="m-menu__ver-arrow la la-angle-right"></i>
											</a>
											<div class="m-menu__submenu ">
												<span class="m-menu__arrow"></span>
												<ul class="m-menu__subnav">
													<li class="m-menu__item " aria-haspopup="true" >
														<a  href="components/buttons/icon/lineawesome.html" class="m-menu__link ">
															<i class="m-menu__link-bullet m-menu__link-bullet--dot">
															<span></span>
															</i>
															<span class="m-menu__link-text">
																Lineawesome Icons
															</span>
														</a>
													</li>
													<li class="m-menu__item " aria-haspopup="true" >
														<a  href="components/buttons/icon/fontawesome.html" class="m-menu__link ">
															<i class="m-menu__link-bullet m-menu__link-bullet--dot">
															<span></span>
															</i>
															<span class="m-menu__link-text">
																Fontawesome Icons
															</span>
														</a>
													</li>
													<li class="m-menu__item " aria-haspopup="true" >
														<a  href="components/buttons/icon/flaticon.html" class="m-menu__link ">
															<i class="m-menu__link-bullet m-menu__link-bullet--dot">
															<span></span>
															</i>
															<span class="m-menu__link-text">
																Flaticon Icons
															</span>
														</a>
													</li>
												</ul>
											</div>
										</li>
									</ul>
								</div>
							</li>
							<li class="m-menu__item  m-menu__item--submenu" aria-haspopup="true"  m-menu-submenu-toggle="hover">
								<a  href="javascript:;" class="m-menu__link m-menu__toggle">
									<i class="m-menu__link-icon flaticon-interface-1"></i>
									<span class="m-menu__link-text">
										Portlets
									</span>
									<i class="m-menu__ver-arrow la la-angle-right"></i>
								</a>
								<div class="m-menu__submenu ">
									<span class="m-menu__arrow"></span>
									<ul class="m-menu__subnav">
										<li class="m-menu__item  m-menu__item--parent" aria-haspopup="true" >
											<span class="m-menu__link">
												<span class="m-menu__link-text">
													Portlets
												</span>
											</span>
										</li>
										<li class="m-menu__item " aria-haspopup="true" >
											<a  href="components/portlets/base.html" class="m-menu__link ">
												<i class="m-menu__link-bullet m-menu__link-bullet--dot">
												<span></span>
												</i>
												<span class="m-menu__link-text">
													Base Portlets
												</span>
											</a>
										</li>
										<li class="m-menu__item " aria-haspopup="true" >
											<a  href="components/portlets/advanced.html" class="m-menu__link ">
												<i class="m-menu__link-bullet m-menu__link-bullet--dot">
												<span></span>
												</i>
												<span class="m-menu__link-text">
													Advanced Portlets
												</span>
											</a>
										</li>
										<li class="m-menu__item " aria-haspopup="true" >
											<a  href="components/portlets/creative.html" class="m-menu__link ">
												<i class="m-menu__link-bullet m-menu__link-bullet--dot">
												<span></span>
												</i>
												<span class="m-menu__link-text">
													Creative Portlets
												</span>
											</a>
										</li>
										<li class="m-menu__item " aria-haspopup="true" >
											<a  href="components/portlets/tabbed.html" class="m-menu__link ">
												<i class="m-menu__link-bullet m-menu__link-bullet--dot">
												<span></span>
												</i>
												<span class="m-menu__link-text">
													Tabbed Portlets
												</span>
											</a>
										</li>
										<li class="m-menu__item " aria-haspopup="true" >
											<a  href="components/portlets/draggable.html" class="m-menu__link ">
												<i class="m-menu__link-bullet m-menu__link-bullet--dot">
												<span></span>
												</i>
												<span class="m-menu__link-text">
													Draggable Portlets
												</span>
											</a>
										</li>
										<li class="m-menu__item " aria-haspopup="true" >
											<a  href="components/portlets/tools.html" class="m-menu__link ">
												<i class="m-menu__link-bullet m-menu__link-bullet--dot">
												<span></span>
												</i>
												<span class="m-menu__link-text">
													Portlet Tools
												</span>
											</a>
										</li>
									</ul>
								</div>
							</li>
							<li class="m-menu__item  m-menu__item--submenu" aria-haspopup="true"  m-menu-submenu-toggle="hover">
								<a  href="javascript:;" class="m-menu__link m-menu__toggle">
									<i class="m-menu__link-icon flaticon-interface-6"></i>
									<span class="m-menu__link-text">
										Timeline
									</span>
									<i class="m-menu__ver-arrow la la-angle-right"></i>
								</a>
								<div class="m-menu__submenu ">
									<span class="m-menu__arrow"></span>
									<ul class="m-menu__subnav">
										<li class="m-menu__item  m-menu__item--parent" aria-haspopup="true" >
											<span class="m-menu__link">
												<span class="m-menu__link-text">
													Timeline
												</span>
											</span>
										</li>
										<li class="m-menu__item " aria-haspopup="true" >
											<a  href="components/timeline/timeline-1.html" class="m-menu__link ">
												<i class="m-menu__link-bullet m-menu__link-bullet--dot">
												<span></span>
												</i>
												<span class="m-menu__link-text">
													Timeline 1
												</span>
											</a>
										</li>
										<li class="m-menu__item " aria-haspopup="true" >
											<a  href="components/timeline/timeline-2.html" class="m-menu__link ">
												<i class="m-menu__link-bullet m-menu__link-bullet--dot">
												<span></span>
												</i>
												<span class="m-menu__link-text">
													Timeline 2
												</span>
											</a>
										</li>
									</ul>
								</div>
							</li>
							<li class="m-menu__item  m-menu__item--submenu" aria-haspopup="true"  m-menu-submenu-toggle="hover">
								<a  href="javascript:;" class="m-menu__link m-menu__toggle">
									<i class="m-menu__link-icon flaticon-network"></i>
									<span class="m-menu__link-text">
										Widgets
									</span>
									<i class="m-menu__ver-arrow la la-angle-right"></i>
								</a>
								<div class="m-menu__submenu ">
									<span class="m-menu__arrow"></span>
									<ul class="m-menu__subnav">
										<li class="m-menu__item  m-menu__item--parent" aria-haspopup="true" >
											<span class="m-menu__link">
												<span class="m-menu__link-text">
													Widgets
												</span>
											</span>
										</li>
										<li class="m-menu__item " aria-haspopup="true" >
											<a  href="components/widgets/general.html" class="m-menu__link ">
												<i class="m-menu__link-bullet m-menu__link-bullet--dot">
												<span></span>
												</i>
												<span class="m-menu__link-text">
													General Widgets
												</span>
											</a>
										</li>
										<li class="m-menu__item " aria-haspopup="true" >
											<a  href="components/widgets/chart.html" class="m-menu__link ">
												<i class="m-menu__link-bullet m-menu__link-bullet--dot">
												<span></span>
												</i>
												<span class="m-menu__link-text">
													Chart Widgets
												</span>
											</a>
										</li>
									</ul>
								</div>
							</li>
							<li class="m-menu__item  m-menu__item--submenu" aria-haspopup="true"  m-menu-submenu-toggle="hover">
								<a  href="javascript:;" class="m-menu__link m-menu__toggle">
									<i class="m-menu__link-icon flaticon-calendar"></i>
									<span class="m-menu__link-text">
										Calendar
									</span>
									<i class="m-menu__ver-arrow la la-angle-right"></i>
								</a>
								<div class="m-menu__submenu ">
									<span class="m-menu__arrow"></span>
									<ul class="m-menu__subnav">
										<li class="m-menu__item  m-menu__item--parent" aria-haspopup="true" >
											<span class="m-menu__link">
												<span class="m-menu__link-text">
													Calendar
												</span>
											</span>
										</li>
										<li class="m-menu__item " aria-haspopup="true" >
											<a  href="components/calendar/basic.html" class="m-menu__link ">
												<i class="m-menu__link-bullet m-menu__link-bullet--dot">
												<span></span>
												</i>
												<span class="m-menu__link-text">
													Basic Calendar
												</span>
											</a>
										</li>
										<li class="m-menu__item " aria-haspopup="true" >
											<a  href="components/calendar/list-view.html" class="m-menu__link ">
												<i class="m-menu__link-bullet m-menu__link-bullet--dot">
												<span></span>
												</i>
												<span class="m-menu__link-text">
													List Views
												</span>
											</a>
										</li>
										<li class="m-menu__item " aria-haspopup="true" >
											<a  href="components/calendar/google.html" class="m-menu__link ">
												<i class="m-menu__link-bullet m-menu__link-bullet--dot">
												<span></span>
												</i>
												<span class="m-menu__link-text">
													Google Calendar
												</span>
											</a>
										</li>
										<li class="m-menu__item " aria-haspopup="true" >
											<a  href="components/calendar/external-events.html" class="m-menu__link ">
												<i class="m-menu__link-bullet m-menu__link-bullet--dot">
												<span></span>
												</i>
												<span class="m-menu__link-text">
													External Events
												</span>
											</a>
										</li>
										<li class="m-menu__item " aria-haspopup="true" >
											<a  href="components/calendar/background-events.html" class="m-menu__link ">
												<i class="m-menu__link-bullet m-menu__link-bullet--dot">
												<span></span>
												</i>
												<span class="m-menu__link-text">
													Background Events
												</span>
											</a>
										</li>
									</ul>
								</div>
							</li>
							<li class="m-menu__item  m-menu__item--submenu" aria-haspopup="true"  m-menu-submenu-toggle="hover">
								<a  href="javascript:;" class="m-menu__link m-menu__toggle">
									<i class="m-menu__link-icon flaticon-diagram"></i>
									<span class="m-menu__link-text">
										Charts
									</span>
									<i class="m-menu__ver-arrow la la-angle-right"></i>
								</a>
								<div class="m-menu__submenu ">
									<span class="m-menu__arrow"></span>
									<ul class="m-menu__subnav">
										<li class="m-menu__item  m-menu__item--parent" aria-haspopup="true" >
											<span class="m-menu__link">
												<span class="m-menu__link-text">
													Charts
												</span>
											</span>
										</li>
										<li class="m-menu__item  m-menu__item--submenu" aria-haspopup="true"  m-menu-submenu-toggle="hover">
											<a  href="javascript:;" class="m-menu__link m-menu__toggle">
												<i class="m-menu__link-bullet m-menu__link-bullet--dot">
												<span></span>
												</i>
												<span class="m-menu__link-text">
													amCharts
												</span>
												<i class="m-menu__ver-arrow la la-angle-right"></i>
											</a>
											<div class="m-menu__submenu ">
												<span class="m-menu__arrow"></span>
												<ul class="m-menu__subnav">
													<li class="m-menu__item " aria-haspopup="true" >
														<a  href="components/charts/amcharts/charts.html" class="m-menu__link ">
															<i class="m-menu__link-bullet m-menu__link-bullet--dot">
															<span></span>
															</i>
															<span class="m-menu__link-text">
																amCharts Charts
															</span>
														</a>
													</li>
													<li class="m-menu__item " aria-haspopup="true" >
														<a  href="components/charts/amcharts/stock-charts.html" class="m-menu__link ">
															<i class="m-menu__link-bullet m-menu__link-bullet--dot">
															<span></span>
															</i>
															<span class="m-menu__link-text">
																amCharts Stock Charts
															</span>
														</a>
													</li>
													<li class="m-menu__item " aria-haspopup="true" >
														<a  href="components/charts/amcharts/maps.html" class="m-menu__link ">
															<i class="m-menu__link-bullet m-menu__link-bullet--dot">
															<span></span>
															</i>
															<span class="m-menu__link-text">
																amCharts Maps
															</span>
														</a>
													</li>
												</ul>
											</div>
										</li>
										<li class="m-menu__item " aria-haspopup="true" >
											<a  href="components/charts/flotcharts.html" class="m-menu__link ">
												<i class="m-menu__link-bullet m-menu__link-bullet--dot">
												<span></span>
												</i>
												<span class="m-menu__link-text">
													Flot Charts
												</span>
											</a>
										</li>
										<li class="m-menu__item " aria-haspopup="true" >
											<a  href="components/charts/google-charts.html" class="m-menu__link ">
												<i class="m-menu__link-bullet m-menu__link-bullet--dot">
												<span></span>
												</i>
												<span class="m-menu__link-text">
													Google Charts
												</span>
											</a>
										</li>
										<li class="m-menu__item " aria-haspopup="true" >
											<a  href="components/charts/morris-charts.html" class="m-menu__link ">
												<i class="m-menu__link-bullet m-menu__link-bullet--dot">
												<span></span>
												</i>
												<span class="m-menu__link-text">
													Morris Charts
												</span>
											</a>
										</li>
									</ul>
								</div>
							</li>
							<li class="m-menu__item  m-menu__item--submenu" aria-haspopup="true"  m-menu-submenu-toggle="hover">
								<a  href="javascript:;" class="m-menu__link m-menu__toggle">
									<i class="m-menu__link-icon flaticon-placeholder-1"></i>
									<span class="m-menu__link-text">
										Maps
									</span>
									<i class="m-menu__ver-arrow la la-angle-right"></i>
								</a>
								<div class="m-menu__submenu ">
									<span class="m-menu__arrow"></span>
									<ul class="m-menu__subnav">
										<li class="m-menu__item  m-menu__item--parent" aria-haspopup="true" >
											<span class="m-menu__link">
												<span class="m-menu__link-text">
													Maps
												</span>
											</span>
										</li>
										<li class="m-menu__item " aria-haspopup="true" >
											<a  href="components/maps/google-maps.html" class="m-menu__link ">
												<i class="m-menu__link-bullet m-menu__link-bullet--dot">
												<span></span>
												</i>
												<span class="m-menu__link-text">
													Google Maps
												</span>
											</a>
										</li>
										<li class="m-menu__item " aria-haspopup="true" >
											<a  href="components/maps/jqvmap.html" class="m-menu__link ">
												<i class="m-menu__link-bullet m-menu__link-bullet--dot">
												<span></span>
												</i>
												<span class="m-menu__link-text">
													JQVMap
												</span>
											</a>
										</li>
									</ul>
								</div>
							</li>
							<li class="m-menu__item  m-menu__item--submenu" aria-haspopup="true"  m-menu-submenu-toggle="hover">
								<a  href="javascript:;" class="m-menu__link m-menu__toggle">
									<i class="m-menu__link-icon flaticon-signs-2"></i>
									<span class="m-menu__link-text">
										Utils
									</span>
									<i class="m-menu__ver-arrow la la-angle-right"></i>
								</a>
								<div class="m-menu__submenu ">
									<span class="m-menu__arrow"></span>
									<ul class="m-menu__subnav">
										<li class="m-menu__item  m-menu__item--parent" aria-haspopup="true" >
											<span class="m-menu__link">
												<span class="m-menu__link-text">
													Utils
												</span>
											</span>
										</li>
										<li class="m-menu__item " aria-haspopup="true" >
											<a  href="components/utils/session-timeout.html" class="m-menu__link ">
												<i class="m-menu__link-bullet m-menu__link-bullet--dot">
												<span></span>
												</i>
												<span class="m-menu__link-text">
													Session Timeout
												</span>
											</a>
										</li>
										<li class="m-menu__item " aria-haspopup="true" >
											<a  href="components/utils/idle-timer.html" class="m-menu__link ">
												<i class="m-menu__link-bullet m-menu__link-bullet--dot">
												<span></span>
												</i>
												<span class="m-menu__link-text">
													Idle Timer
												</span>
											</a>
										</li>
									</ul>
								</div>
							</li>
							<li class="m-menu__section ">
								<h4 class="m-menu__section-text">
								CRUD
								</h4>
								<i class="m-menu__section-icon flaticon-more-v3"></i>
							</li>
							<li class="m-menu__item  m-menu__item--submenu" aria-haspopup="true"  m-menu-submenu-toggle="hover">
								<a  href="javascript:;" class="m-menu__link m-menu__toggle">
									<i class="m-menu__link-icon flaticon-interface-7"></i>
									<span class="m-menu__link-text">
										Forms & Controls
									</span>
									<i class="m-menu__ver-arrow la la-angle-right"></i>
								</a>
								<div class="m-menu__submenu ">
									<span class="m-menu__arrow"></span>
									<ul class="m-menu__subnav">
										<li class="m-menu__item  m-menu__item--parent" aria-haspopup="true" >
											<span class="m-menu__link">
												<span class="m-menu__link-text">
													Forms & Controls
												</span>
											</span>
										</li>
										<li class="m-menu__item  m-menu__item--submenu" aria-haspopup="true"  m-menu-submenu-toggle="hover">
											<a  href="javascript:;" class="m-menu__link m-menu__toggle">
												<i class="m-menu__link-bullet m-menu__link-bullet--dot">
												<span></span>
												</i>
												<span class="m-menu__link-text">
													Form Controls
												</span>
												<i class="m-menu__ver-arrow la la-angle-right"></i>
											</a>
											<div class="m-menu__submenu ">
												<span class="m-menu__arrow"></span>
												<ul class="m-menu__subnav">
													<li class="m-menu__item " aria-haspopup="true" >
														<a  href="crud/forms/controls/base.html" class="m-menu__link ">
															<i class="m-menu__link-bullet m-menu__link-bullet--dot">
															<span></span>
															</i>
															<span class="m-menu__link-text">
																Base Inputs
															</span>
														</a>
													</li>
													<li class="m-menu__item " aria-haspopup="true" >
														<a  href="crud/forms/controls/checkbox-radio.html" class="m-menu__link ">
															<i class="m-menu__link-bullet m-menu__link-bullet--dot">
															<span></span>
															</i>
															<span class="m-menu__link-text">
																Checkbox & Radio
															</span>
														</a>
													</li>
													<li class="m-menu__item " aria-haspopup="true" >
														<a  href="crud/forms/controls/input-group.html" class="m-menu__link ">
															<i class="m-menu__link-bullet m-menu__link-bullet--dot">
															<span></span>
															</i>
															<span class="m-menu__link-text">
																Input Groups
															</span>
														</a>
													</li>
													<li class="m-menu__item " aria-haspopup="true" >
														<a  href="crud/forms/controls/switch.html" class="m-menu__link ">
															<i class="m-menu__link-bullet m-menu__link-bullet--dot">
															<span></span>
															</i>
															<span class="m-menu__link-text">
																Switch
															</span>
														</a>
													</li>
													<li class="m-menu__item " aria-haspopup="true" >
														<a  href="crud/forms/controls/option.html" class="m-menu__link ">
															<i class="m-menu__link-bullet m-menu__link-bullet--dot">
															<span></span>
															</i>
															<span class="m-menu__link-text">
																Mega Options
															</span>
														</a>
													</li>
												</ul>
											</div>
										</li>
										<li class="m-menu__item  m-menu__item--submenu" aria-haspopup="true"  m-menu-submenu-toggle="hover">
											<a  href="javascript:;" class="m-menu__link m-menu__toggle">
												<i class="m-menu__link-bullet m-menu__link-bullet--dot">
												<span></span>
												</i>
												<span class="m-menu__link-text">
													Form Widgets
												</span>
												<i class="m-menu__ver-arrow la la-angle-right"></i>
											</a>
											<div class="m-menu__submenu ">
												<span class="m-menu__arrow"></span>
												<ul class="m-menu__subnav">
													<li class="m-menu__item " aria-haspopup="true" >
														<a  href="crud/forms/widgets/bootstrap-datepicker.html" class="m-menu__link ">
															<i class="m-menu__link-bullet m-menu__link-bullet--dot">
															<span></span>
															</i>
															<span class="m-menu__link-text">
																Datepicker
															</span>
														</a>
													</li>
													<li class="m-menu__item " aria-haspopup="true" >
														<a  href="crud/forms/widgets/bootstrap-datetimepicker.html" class="m-menu__link ">
															<i class="m-menu__link-bullet m-menu__link-bullet--dot">
															<span></span>
															</i>
															<span class="m-menu__link-text">
																Datetimepicker
															</span>
														</a>
													</li>
													<li class="m-menu__item " aria-haspopup="true" >
														<a  href="crud/forms/widgets/bootstrap-timepicker.html" class="m-menu__link ">
															<i class="m-menu__link-bullet m-menu__link-bullet--dot">
															<span></span>
															</i>
															<span class="m-menu__link-text">
																Timepicker
															</span>
														</a>
													</li>
													<li class="m-menu__item " aria-haspopup="true" >
														<a  href="crud/forms/widgets/bootstrap-daterangepicker.html" class="m-menu__link ">
															<i class="m-menu__link-bullet m-menu__link-bullet--dot">
															<span></span>
															</i>
															<span class="m-menu__link-text">
																Daterangepicker
															</span>
														</a>
													</li>
													<li class="m-menu__item " aria-haspopup="true" >
														<a  href="crud/forms/widgets/bootstrap-touchspin.html" class="m-menu__link ">
															<i class="m-menu__link-bullet m-menu__link-bullet--dot">
															<span></span>
															</i>
															<span class="m-menu__link-text">
																Touchspin
															</span>
														</a>
													</li>
													<li class="m-menu__item " aria-haspopup="true" >
														<a  href="crud/forms/widgets/bootstrap-maxlength.html" class="m-menu__link ">
															<i class="m-menu__link-bullet m-menu__link-bullet--dot">
															<span></span>
															</i>
															<span class="m-menu__link-text">
																Maxlength
															</span>
														</a>
													</li>
													<li class="m-menu__item " aria-haspopup="true" >
														<a  href="crud/forms/widgets/bootstrap-switch.html" class="m-menu__link ">
															<i class="m-menu__link-bullet m-menu__link-bullet--dot">
															<span></span>
															</i>
															<span class="m-menu__link-text">
																Switch
															</span>
														</a>
													</li>
													<li class="m-menu__item " aria-haspopup="true" >
														<a  href="crud/forms/widgets/bootstrap-multipleselectsplitter.html" class="m-menu__link ">
															<i class="m-menu__link-bullet m-menu__link-bullet--dot">
															<span></span>
															</i>
															<span class="m-menu__link-text">
																Multiple Select Splitter
															</span>
														</a>
													</li>
													<li class="m-menu__item " aria-haspopup="true" >
														<a  href="crud/forms/widgets/bootstrap-select.html" class="m-menu__link ">
															<i class="m-menu__link-bullet m-menu__link-bullet--dot">
															<span></span>
															</i>
															<span class="m-menu__link-text">
																Bootstrap Select
															</span>
														</a>
													</li>
													<li class="m-menu__item " aria-haspopup="true" >
														<a  href="crud/forms/widgets/select2.html" class="m-menu__link ">
															<i class="m-menu__link-bullet m-menu__link-bullet--dot">
															<span></span>
															</i>
															<span class="m-menu__link-text">
																Select2
															</span>
														</a>
													</li>
													<li class="m-menu__item " aria-haspopup="true" >
														<a  href="crud/forms/widgets/typeahead.html" class="m-menu__link ">
															<i class="m-menu__link-bullet m-menu__link-bullet--dot">
															<span></span>
															</i>
															<span class="m-menu__link-text">
																Typeahead
															</span>
														</a>
													</li>
													<li class="m-menu__item " aria-haspopup="true" >
														<a  href="crud/forms/widgets/nouislider.html" class="m-menu__link ">
															<i class="m-menu__link-bullet m-menu__link-bullet--dot">
															<span></span>
															</i>
															<span class="m-menu__link-text">
																noUiSlider
															</span>
														</a>
													</li>
													<li class="m-menu__item " aria-haspopup="true" >
														<a  href="crud/forms/widgets/form-repeater.html" class="m-menu__link ">
															<i class="m-menu__link-bullet m-menu__link-bullet--dot">
															<span></span>
															</i>
															<span class="m-menu__link-text">
																Form Repeater
															</span>
														</a>
													</li>
													<li class="m-menu__item " aria-haspopup="true" >
														<a  href="crud/forms/widgets/ion-range-slider.html" class="m-menu__link ">
															<i class="m-menu__link-bullet m-menu__link-bullet--dot">
															<span></span>
															</i>
															<span class="m-menu__link-text">
																Ion Range Slider
															</span>
														</a>
													</li>
													<li class="m-menu__item " aria-haspopup="true" >
														<a  href="crud/forms/widgets/input-mask.html" class="m-menu__link ">
															<i class="m-menu__link-bullet m-menu__link-bullet--dot">
															<span></span>
															</i>
															<span class="m-menu__link-text">
																Input Masks
															</span>
														</a>
													</li>
													<li class="m-menu__item " aria-haspopup="true" >
														<a  href="crud/forms/widgets/summernote.html" class="m-menu__link ">
															<i class="m-menu__link-bullet m-menu__link-bullet--dot">
															<span></span>
															</i>
															<span class="m-menu__link-text">
																Summernote WYSIWYG
															</span>
														</a>
													</li>
													<li class="m-menu__item " aria-haspopup="true" >
														<a  href="crud/forms/widgets/bootstrap-markdown.html" class="m-menu__link ">
															<i class="m-menu__link-bullet m-menu__link-bullet--dot">
															<span></span>
															</i>
															<span class="m-menu__link-text">
																Markdown Editor
															</span>
														</a>
													</li>
													<li class="m-menu__item " aria-haspopup="true" >
														<a  href="crud/forms/widgets/autosize.html" class="m-menu__link ">
															<i class="m-menu__link-bullet m-menu__link-bullet--dot">
															<span></span>
															</i>
															<span class="m-menu__link-text">
																Autosize
															</span>
														</a>
													</li>
													<li class="m-menu__item " aria-haspopup="true" >
														<a  href="crud/forms/widgets/clipboard.html" class="m-menu__link ">
															<i class="m-menu__link-bullet m-menu__link-bullet--dot">
															<span></span>
															</i>
															<span class="m-menu__link-text">
																Clipboard
															</span>
														</a>
													</li>
													<li class="m-menu__item " aria-haspopup="true" >
														<a  href="crud/forms/widgets/dropzone.html" class="m-menu__link ">
															<i class="m-menu__link-bullet m-menu__link-bullet--dot">
															<span></span>
															</i>
															<span class="m-menu__link-text">
																Dropzone
															</span>
														</a>
													</li>
													<li class="m-menu__item " aria-haspopup="true" >
														<a  href="crud/forms/widgets/recaptcha.html" class="m-menu__link ">
															<i class="m-menu__link-bullet m-menu__link-bullet--dot">
															<span></span>
															</i>
															<span class="m-menu__link-text">
																Google reCaptcha
															</span>
														</a>
													</li>
												</ul>
											</div>
										</li>
										<li class="m-menu__item  m-menu__item--submenu" aria-haspopup="true"  m-menu-submenu-toggle="hover">
											<a  href="javascript:;" class="m-menu__link m-menu__toggle">
												<i class="m-menu__link-bullet m-menu__link-bullet--dot">
												<span></span>
												</i>
												<span class="m-menu__link-text">
													Form Layouts
												</span>
												<i class="m-menu__ver-arrow la la-angle-right"></i>
											</a>
											<div class="m-menu__submenu ">
												<span class="m-menu__arrow"></span>
												<ul class="m-menu__subnav">
													<li class="m-menu__item " aria-haspopup="true" >
														<a  href="crud/forms/layouts/default-forms.html" class="m-menu__link ">
															<i class="m-menu__link-bullet m-menu__link-bullet--dot">
															<span></span>
															</i>
															<span class="m-menu__link-text">
																Default Forms
															</span>
														</a>
													</li>
													<li class="m-menu__item " aria-haspopup="true" >
														<a  href="crud/forms/layouts/multi-column-forms.html" class="m-menu__link ">
															<i class="m-menu__link-bullet m-menu__link-bullet--dot">
															<span></span>
															</i>
															<span class="m-menu__link-text">
																Multi Column Forms
															</span>
														</a>
													</li>
													<li class="m-menu__item " aria-haspopup="true" >
														<a  href="crud/forms/layouts/action-bars.html" class="m-menu__link ">
															<i class="m-menu__link-bullet m-menu__link-bullet--dot">
															<span></span>
															</i>
															<span class="m-menu__link-text">
																Action Bars
															</span>
														</a>
													</li>
												</ul>
											</div>
										</li>
										<li class="m-menu__item  m-menu__item--submenu" aria-haspopup="true"  m-menu-submenu-toggle="hover">
											<a  href="javascript:;" class="m-menu__link m-menu__toggle">
												<i class="m-menu__link-bullet m-menu__link-bullet--dot">
												<span></span>
												</i>
												<span class="m-menu__link-text">
													Form Validation
												</span>
												<i class="m-menu__ver-arrow la la-angle-right"></i>
											</a>
											<div class="m-menu__submenu ">
												<span class="m-menu__arrow"></span>
												<ul class="m-menu__subnav">
													<li class="m-menu__item " aria-haspopup="true" >
														<a  href="crud/forms/validation/states.html" class="m-menu__link ">
															<i class="m-menu__link-bullet m-menu__link-bullet--dot">
															<span></span>
															</i>
															<span class="m-menu__link-text">
																Validation States
															</span>
														</a>
													</li>
													<li class="m-menu__item " aria-haspopup="true" >
														<a  href="crud/forms/validation/form-controls.html" class="m-menu__link ">
															<i class="m-menu__link-bullet m-menu__link-bullet--dot">
															<span></span>
															</i>
															<span class="m-menu__link-text">
																Form Controls
															</span>
														</a>
													</li>
													<li class="m-menu__item " aria-haspopup="true" >
														<a  href="crud/forms/validation/form-widgets.html" class="m-menu__link ">
															<i class="m-menu__link-bullet m-menu__link-bullet--dot">
															<span></span>
															</i>
															<span class="m-menu__link-text">
																Form Widgets
															</span>
														</a>
													</li>
												</ul>
											</div>
										</li>
									</ul>
								</div>
							</li>
							<li class="m-menu__item  m-menu__item--submenu" aria-haspopup="true"  m-menu-submenu-toggle="hover">
								<a  href="javascript:;" class="m-menu__link m-menu__toggle">
									<i class="m-menu__link-icon flaticon-interface-8"></i>
									<span class="m-menu__link-text">
										Metronic Wizard
									</span>
									<i class="m-menu__ver-arrow la la-angle-right"></i>
								</a>
								<div class="m-menu__submenu ">
									<span class="m-menu__arrow"></span>
									<ul class="m-menu__subnav">
										<li class="m-menu__item " aria-haspopup="true" >
											<a  href="crud/wizard/wizard-1.html" class="m-menu__link ">
												<i class="m-menu__link-bullet m-menu__link-bullet--dot">
												<span></span>
												</i>
												<span class="m-menu__link-text">
													Form Wizard 1
												</span>
											</a>
										</li>
										<li class="m-menu__item " aria-haspopup="true" >
											<a  href="crud/wizard/wizard-2.html" class="m-menu__link ">
												<i class="m-menu__link-bullet m-menu__link-bullet--dot">
												<span></span>
												</i>
												<span class="m-menu__link-text">
													Form Wizard 2
												</span>
											</a>
										</li>
										<li class="m-menu__item " aria-haspopup="true" >
											<a  href="crud/wizard/wizard-3.html" class="m-menu__link ">
												<i class="m-menu__link-bullet m-menu__link-bullet--dot">
												<span></span>
												</i>
												<span class="m-menu__link-text">
													Form Wizard 3
												</span>
											</a>
										</li>
										<li class="m-menu__item " aria-haspopup="true" >
											<a  href="crud/wizard/wizard-4.html" class="m-menu__link ">
												<i class="m-menu__link-bullet m-menu__link-bullet--dot">
												<span></span>
												</i>
												<span class="m-menu__link-text">
													Form Wizard 4
												</span>
											</a>
										</li>
										<li class="m-menu__item " aria-haspopup="true" >
											<a  href="crud/wizard/wizard-5.html" class="m-menu__link ">
												<i class="m-menu__link-bullet m-menu__link-bullet--dot">
												<span></span>
												</i>
												<span class="m-menu__link-text">
													Form Wizard 5
												</span>
											</a>
										</li>
									</ul>
								</div>
							</li>
							<li class="m-menu__item  m-menu__item--submenu" aria-haspopup="true"  m-menu-submenu-toggle="hover">
								<a  href="javascript:;" class="m-menu__link m-menu__toggle">
									<i class="m-menu__link-icon flaticon-tabs"></i>
									<span class="m-menu__link-text">
										Metronic Datatable
									</span>
									<i class="m-menu__ver-arrow la la-angle-right"></i>
								</a>
								<div class="m-menu__submenu ">
									<span class="m-menu__arrow"></span>
									<ul class="m-menu__subnav">
										<li class="m-menu__item  m-menu__item--parent" aria-haspopup="true" >
											<span class="m-menu__link">
												<span class="m-menu__link-text">
													Metronic Datatable
												</span>
											</span>
										</li>
										<li class="m-menu__item  m-menu__item--submenu" aria-haspopup="true"  m-menu-submenu-toggle="hover">
											<a  href="javascript:;" class="m-menu__link m-menu__toggle">
												<i class="m-menu__link-bullet m-menu__link-bullet--dot">
												<span></span>
												</i>
												<span class="m-menu__link-text">
													Base
												</span>
												<i class="m-menu__ver-arrow la la-angle-right"></i>
											</a>
											<div class="m-menu__submenu ">
												<span class="m-menu__arrow"></span>
												<ul class="m-menu__subnav">
													<li class="m-menu__item " aria-haspopup="true" >
														<a  href="crud/metronic-datatable/base/data-local.html" class="m-menu__link ">
															<i class="m-menu__link-bullet m-menu__link-bullet--dot">
															<span></span>
															</i>
															<span class="m-menu__link-text">
																Local Data
															</span>
														</a>
													</li>
													<li class="m-menu__item " aria-haspopup="true" >
														<a  href="crud/metronic-datatable/base/data-json.html" class="m-menu__link ">
															<i class="m-menu__link-bullet m-menu__link-bullet--dot">
															<span></span>
															</i>
															<span class="m-menu__link-text">
																JSON Data
															</span>
														</a>
													</li>
													<li class="m-menu__item " aria-haspopup="true" >
														<a  href="crud/metronic-datatable/base/data-ajax.html" class="m-menu__link ">
															<i class="m-menu__link-bullet m-menu__link-bullet--dot">
															<span></span>
															</i>
															<span class="m-menu__link-text">
																Ajax Data
															</span>
														</a>
													</li>
													<li class="m-menu__item " aria-haspopup="true" >
														<a  href="crud/metronic-datatable/base/html-table.html" class="m-menu__link ">
															<i class="m-menu__link-bullet m-menu__link-bullet--dot">
															<span></span>
															</i>
															<span class="m-menu__link-text">
																HTML Table
															</span>
														</a>
													</li>
													<li class="m-menu__item " aria-haspopup="true" >
														<a  href="crud/metronic-datatable/base/record-selection.html" class="m-menu__link ">
															<i class="m-menu__link-bullet m-menu__link-bullet--dot">
															<span></span>
															</i>
															<span class="m-menu__link-text">
																Record Selection
															</span>
														</a>
													</li>
													<li class="m-menu__item " aria-haspopup="true" >
														<a  href="crud/metronic-datatable/base/local-sort.html" class="m-menu__link ">
															<i class="m-menu__link-bullet m-menu__link-bullet--dot">
															<span></span>
															</i>
															<span class="m-menu__link-text">
																Local Sort
															</span>
														</a>
													</li>
													<li class="m-menu__item " aria-haspopup="true" >
														<a  href="crud/metronic-datatable/base/row-details.html" class="m-menu__link ">
															<i class="m-menu__link-bullet m-menu__link-bullet--dot">
															<span></span>
															</i>
															<span class="m-menu__link-text">
																Row Details
															</span>
														</a>
													</li>
													<li class="m-menu__item " aria-haspopup="true" >
														<a  href="crud/metronic-datatable/base/column-rendering.html" class="m-menu__link ">
															<i class="m-menu__link-bullet m-menu__link-bullet--dot">
															<span></span>
															</i>
															<span class="m-menu__link-text">
																Column Rendering
															</span>
														</a>
													</li>
													<li class="m-menu__item " aria-haspopup="true" >
														<a  href="crud/metronic-datatable/base/column-width.html" class="m-menu__link ">
															<i class="m-menu__link-bullet m-menu__link-bullet--dot">
															<span></span>
															</i>
															<span class="m-menu__link-text">
																Column Width
															</span>
														</a>
													</li>
													<li class="m-menu__item " aria-haspopup="true" >
														<a  href="crud/metronic-datatable/base/responsive-columns.html" class="m-menu__link ">
															<i class="m-menu__link-bullet m-menu__link-bullet--dot">
															<span></span>
															</i>
															<span class="m-menu__link-text">
																Responsive Columns
															</span>
														</a>
													</li>
													<li class="m-menu__item " aria-haspopup="true" >
														<a  href="crud/metronic-datatable/base/translation.html" class="m-menu__link ">
															<i class="m-menu__link-bullet m-menu__link-bullet--dot">
															<span></span>
															</i>
															<span class="m-menu__link-text">
																Translation
															</span>
														</a>
													</li>
												</ul>
											</div>
										</li>
										<li class="m-menu__item  m-menu__item--submenu" aria-haspopup="true"  m-menu-submenu-toggle="hover">
											<a  href="javascript:;" class="m-menu__link m-menu__toggle">
												<i class="m-menu__link-bullet m-menu__link-bullet--dot">
												<span></span>
												</i>
												<span class="m-menu__link-text">
													Scrolling
												</span>
												<i class="m-menu__ver-arrow la la-angle-right"></i>
											</a>
											<div class="m-menu__submenu ">
												<span class="m-menu__arrow"></span>
												<ul class="m-menu__subnav">
													<li class="m-menu__item " aria-haspopup="true" >
														<a  href="crud/metronic-datatable/scrolling/vertical.html" class="m-menu__link ">
															<i class="m-menu__link-bullet m-menu__link-bullet--dot">
															<span></span>
															</i>
															<span class="m-menu__link-text">
																Vertical Scrolling
															</span>
														</a>
													</li>
													<li class="m-menu__item " aria-haspopup="true" >
														<a  href="crud/metronic-datatable/scrolling/horizontal.html" class="m-menu__link ">
															<i class="m-menu__link-bullet m-menu__link-bullet--dot">
															<span></span>
															</i>
															<span class="m-menu__link-text">
																Horizontal Scrolling
															</span>
														</a>
													</li>
													<li class="m-menu__item " aria-haspopup="true" >
														<a  href="crud/metronic-datatable/scrolling/both.html" class="m-menu__link ">
															<i class="m-menu__link-bullet m-menu__link-bullet--dot">
															<span></span>
															</i>
															<span class="m-menu__link-text">
																Both Scrolling
															</span>
														</a>
													</li>
												</ul>
											</div>
										</li>
										<li class="m-menu__item  m-menu__item--submenu" aria-haspopup="true"  m-menu-submenu-toggle="hover">
											<a  href="javascript:;" class="m-menu__link m-menu__toggle">
												<i class="m-menu__link-bullet m-menu__link-bullet--dot">
												<span></span>
												</i>
												<span class="m-menu__link-text">
													Locked Columns
												</span>
												<i class="m-menu__ver-arrow la la-angle-right"></i>
											</a>
											<div class="m-menu__submenu ">
												<span class="m-menu__arrow"></span>
												<ul class="m-menu__subnav">
													<li class="m-menu__item " aria-haspopup="true" >
														<a  href="crud/metronic-datatable/locked/left.html" class="m-menu__link ">
															<i class="m-menu__link-bullet m-menu__link-bullet--dot">
															<span></span>
															</i>
															<span class="m-menu__link-text">
																Left Locked Columns
															</span>
														</a>
													</li>
													<li class="m-menu__item " aria-haspopup="true" >
														<a  href="crud/metronic-datatable/locked/right.html" class="m-menu__link ">
															<i class="m-menu__link-bullet m-menu__link-bullet--dot">
															<span></span>
															</i>
															<span class="m-menu__link-text">
																Right Locked Columns
															</span>
														</a>
													</li>
													<li class="m-menu__item " aria-haspopup="true" >
														<a  href="crud/metronic-datatable/locked/both.html" class="m-menu__link ">
															<i class="m-menu__link-bullet m-menu__link-bullet--dot">
															<span></span>
															</i>
															<span class="m-menu__link-text">
																Both Locked Columns
															</span>
														</a>
													</li>
													<li class="m-menu__item " aria-haspopup="true" >
														<a  href="crud/metronic-datatable/locked/html-table.html" class="m-menu__link ">
															<i class="m-menu__link-bullet m-menu__link-bullet--dot">
															<span></span>
															</i>
															<span class="m-menu__link-text">
																HTML Table
															</span>
														</a>
													</li>
												</ul>
											</div>
										</li>
										<li class="m-menu__item  m-menu__item--submenu" aria-haspopup="true"  m-menu-submenu-toggle="hover">
											<a  href="javascript:;" class="m-menu__link m-menu__toggle">
												<i class="m-menu__link-bullet m-menu__link-bullet--dot">
												<span></span>
												</i>
												<span class="m-menu__link-text">
													Child Datatables
												</span>
												<i class="m-menu__ver-arrow la la-angle-right"></i>
											</a>
											<div class="m-menu__submenu ">
												<span class="m-menu__arrow"></span>
												<ul class="m-menu__subnav">
													<li class="m-menu__item " aria-haspopup="true" >
														<a  href="crud/metronic-datatable/child/data-local.html" class="m-menu__link ">
															<i class="m-menu__link-bullet m-menu__link-bullet--dot">
															<span></span>
															</i>
															<span class="m-menu__link-text">
																Local Data
															</span>
														</a>
													</li>
													<li class="m-menu__item " aria-haspopup="true" >
														<a  href="crud/metronic-datatable/child/data-ajax.html" class="m-menu__link ">
															<i class="m-menu__link-bullet m-menu__link-bullet--dot">
															<span></span>
															</i>
															<span class="m-menu__link-text">
																Remote Data
															</span>
														</a>
													</li>
												</ul>
											</div>
										</li>
										<li class="m-menu__item  m-menu__item--submenu" aria-haspopup="true"  m-menu-submenu-toggle="hover">
											<a  href="javascript:;" class="m-menu__link m-menu__toggle">
												<i class="m-menu__link-bullet m-menu__link-bullet--dot">
												<span></span>
												</i>
												<span class="m-menu__link-text">
													API
												</span>
												<i class="m-menu__ver-arrow la la-angle-right"></i>
											</a>
											<div class="m-menu__submenu ">
												<span class="m-menu__arrow"></span>
												<ul class="m-menu__subnav">
													<li class="m-menu__item " aria-haspopup="true" >
														<a  href="crud/metronic-datatable/api/methods.html" class="m-menu__link ">
															<i class="m-menu__link-bullet m-menu__link-bullet--dot">
															<span></span>
															</i>
															<span class="m-menu__link-text">
																API Methods
															</span>
														</a>
													</li>
													<li class="m-menu__item " aria-haspopup="true" >
														<a  href="crud/metronic-datatable/api/events.html" class="m-menu__link ">
															<i class="m-menu__link-bullet m-menu__link-bullet--dot">
															<span></span>
															</i>
															<span class="m-menu__link-text">
																Events
															</span>
														</a>
													</li>
												</ul>
											</div>
										</li>
									</ul>
								</div>
							</li>
							<li class="m-menu__item  m-menu__item--submenu" aria-haspopup="true"  m-menu-submenu-toggle="hover">
								<a  href="javascript:;" class="m-menu__link m-menu__toggle">
									<i class="m-menu__link-icon flaticon-list-3"></i>
									<span class="m-menu__link-text">
										DataTables
									</span>
									<i class="m-menu__ver-arrow la la-angle-right"></i>
								</a>
								<div class="m-menu__submenu ">
									<span class="m-menu__arrow"></span>
									<ul class="m-menu__subnav">
										<li class="m-menu__item  m-menu__item--parent" aria-haspopup="true" >
											<span class="m-menu__link">
												<span class="m-menu__link-text">
													DataTables
												</span>
											</span>
										</li>
										<li class="m-menu__item  m-menu__item--submenu" aria-haspopup="true"  m-menu-submenu-toggle="hover">
											<a  href="javascript:;" class="m-menu__link m-menu__toggle">
												<i class="m-menu__link-bullet m-menu__link-bullet--dot">
												<span></span>
												</i>
												<span class="m-menu__link-text">
													Basic
												</span>
												<i class="m-menu__ver-arrow la la-angle-right"></i>
											</a>
											<div class="m-menu__submenu ">
												<span class="m-menu__arrow"></span>
												<ul class="m-menu__subnav">
													<li class="m-menu__item " aria-haspopup="true" >
														<a  href="crud/datatables/basic/basic.html" class="m-menu__link ">
															<i class="m-menu__link-bullet m-menu__link-bullet--dot">
															<span></span>
															</i>
															<span class="m-menu__link-text">
																Basic Tables
															</span>
														</a>
													</li>
													<li class="m-menu__item " aria-haspopup="true" >
														<a  href="crud/datatables/basic/scrollable.html" class="m-menu__link ">
															<i class="m-menu__link-bullet m-menu__link-bullet--dot">
															<span></span>
															</i>
															<span class="m-menu__link-text">
																Scrollable Tables
															</span>
														</a>
													</li>
													<li class="m-menu__item " aria-haspopup="true" >
														<a  href="crud/datatables/basic/headers.html" class="m-menu__link ">
															<i class="m-menu__link-bullet m-menu__link-bullet--dot">
															<span></span>
															</i>
															<span class="m-menu__link-text">
																Complex Headers
															</span>
														</a>
													</li>
													<li class="m-menu__item " aria-haspopup="true" >
														<a  href="crud/datatables/basic/paginations.html" class="m-menu__link ">
															<i class="m-menu__link-bullet m-menu__link-bullet--dot">
															<span></span>
															</i>
															<span class="m-menu__link-text">
																Pagination Options
															</span>
														</a>
													</li>
												</ul>
											</div>
										</li>
										<li class="m-menu__item  m-menu__item--submenu" aria-haspopup="true"  m-menu-submenu-toggle="hover">
											<a  href="javascript:;" class="m-menu__link m-menu__toggle">
												<i class="m-menu__link-bullet m-menu__link-bullet--dot">
												<span></span>
												</i>
												<span class="m-menu__link-text">
													Advanced
												</span>
												<i class="m-menu__ver-arrow la la-angle-right"></i>
											</a>
											<div class="m-menu__submenu ">
												<span class="m-menu__arrow"></span>
												<ul class="m-menu__subnav">
													<li class="m-menu__item " aria-haspopup="true" >
														<a  href="crud/datatables/advanced/column-rendering.html" class="m-menu__link ">
															<i class="m-menu__link-bullet m-menu__link-bullet--dot">
															<span></span>
															</i>
															<span class="m-menu__link-text">
																Column Rendering
															</span>
														</a>
													</li>
													<li class="m-menu__item " aria-haspopup="true" >
														<a  href="crud/datatables/advanced/multiple-controls.html" class="m-menu__link ">
															<i class="m-menu__link-bullet m-menu__link-bullet--dot">
															<span></span>
															</i>
															<span class="m-menu__link-text">
																Multiple Controls
															</span>
														</a>
													</li>
													<li class="m-menu__item " aria-haspopup="true" >
														<a  href="crud/datatables/advanced/column-visibility.html" class="m-menu__link ">
															<i class="m-menu__link-bullet m-menu__link-bullet--dot">
															<span></span>
															</i>
															<span class="m-menu__link-text">
																Column Visibility
															</span>
														</a>
													</li>
													<li class="m-menu__item " aria-haspopup="true" >
														<a  href="crud/datatables/advanced/row-callback.html" class="m-menu__link ">
															<i class="m-menu__link-bullet m-menu__link-bullet--dot">
															<span></span>
															</i>
															<span class="m-menu__link-text">
																Row Callback
															</span>
														</a>
													</li>
													<li class="m-menu__item " aria-haspopup="true" >
														<a  href="crud/datatables/advanced/row-grouping.html" class="m-menu__link ">
															<i class="m-menu__link-bullet m-menu__link-bullet--dot">
															<span></span>
															</i>
															<span class="m-menu__link-text">
																Row Grouping
															</span>
														</a>
													</li>
													<li class="m-menu__item " aria-haspopup="true" >
														<a  href="crud/datatables/advanced/footer-callback.html" class="m-menu__link ">
															<i class="m-menu__link-bullet m-menu__link-bullet--dot">
															<span></span>
															</i>
															<span class="m-menu__link-text">
																Footer Callback
															</span>
														</a>
													</li>
												</ul>
											</div>
										</li>
										<li class="m-menu__item  m-menu__item--submenu" aria-haspopup="true"  m-menu-submenu-toggle="hover">
											<a  href="javascript:;" class="m-menu__link m-menu__toggle">
												<i class="m-menu__link-bullet m-menu__link-bullet--dot">
												<span></span>
												</i>
												<span class="m-menu__link-text">
													Data sources
												</span>
												<i class="m-menu__ver-arrow la la-angle-right"></i>
											</a>
											<div class="m-menu__submenu ">
												<span class="m-menu__arrow"></span>
												<ul class="m-menu__subnav">
													<li class="m-menu__item " aria-haspopup="true" >
														<a  href="crud/datatables/data-sources/html.html" class="m-menu__link ">
															<i class="m-menu__link-bullet m-menu__link-bullet--dot">
															<span></span>
															</i>
															<span class="m-menu__link-text">
																HTML
															</span>
														</a>
													</li>
													<li class="m-menu__item " aria-haspopup="true" >
														<a  href="crud/datatables/data-sources/javascript.html" class="m-menu__link ">
															<i class="m-menu__link-bullet m-menu__link-bullet--dot">
															<span></span>
															</i>
															<span class="m-menu__link-text">
																Javascript
															</span>
														</a>
													</li>
													<li class="m-menu__item " aria-haspopup="true" >
														<a  href="crud/datatables/data-sources/ajax-client-side.html" class="m-menu__link ">
															<i class="m-menu__link-bullet m-menu__link-bullet--dot">
															<span></span>
															</i>
															<span class="m-menu__link-text">
																Ajax Client-side
															</span>
														</a>
													</li>
													<li class="m-menu__item " aria-haspopup="true" >
														<a  href="crud/datatables/data-sources/ajax-server-side.html" class="m-menu__link ">
															<i class="m-menu__link-bullet m-menu__link-bullet--dot">
															<span></span>
															</i>
															<span class="m-menu__link-text">
																Ajax Server-side
															</span>
														</a>
													</li>
												</ul>
											</div>
										</li>
										<li class="m-menu__item  m-menu__item--submenu" aria-haspopup="true"  m-menu-submenu-toggle="hover">
											<a  href="javascript:;" class="m-menu__link m-menu__toggle">
												<i class="m-menu__link-bullet m-menu__link-bullet--dot">
												<span></span>
												</i>
												<span class="m-menu__link-text">
													Search Options
												</span>
												<i class="m-menu__ver-arrow la la-angle-right"></i>
											</a>
											<div class="m-menu__submenu ">
												<span class="m-menu__arrow"></span>
												<ul class="m-menu__subnav">
													<li class="m-menu__item " aria-haspopup="true" >
														<a  href="crud/datatables/search-options/column-search.html" class="m-menu__link ">
															<i class="m-menu__link-bullet m-menu__link-bullet--dot">
															<span></span>
															</i>
															<span class="m-menu__link-text">
																Column Search
															</span>
														</a>
													</li>
													<li class="m-menu__item " aria-haspopup="true" >
														<a  href="crud/datatables/search-options/advanced-search.html" class="m-menu__link ">
															<i class="m-menu__link-bullet m-menu__link-bullet--dot">
															<span></span>
															</i>
															<span class="m-menu__link-text">
																Advanced Search
															</span>
														</a>
													</li>
												</ul>
											</div>
										</li>
										<li class="m-menu__item  m-menu__item--submenu" aria-haspopup="true"  m-menu-submenu-toggle="hover">
											<a  href="javascript:;" class="m-menu__link m-menu__toggle">
												<i class="m-menu__link-bullet m-menu__link-bullet--dot">
												<span></span>
												</i>
												<span class="m-menu__link-text">
													Extensions
												</span>
												<i class="m-menu__ver-arrow la la-angle-right"></i>
											</a>
											<div class="m-menu__submenu ">
												<span class="m-menu__arrow"></span>
												<ul class="m-menu__subnav">
													<li class="m-menu__item " aria-haspopup="true" >
														<a  href="crud/datatables/extensions/buttons.html" class="m-menu__link ">
															<i class="m-menu__link-bullet m-menu__link-bullet--dot">
															<span></span>
															</i>
															<span class="m-menu__link-text">
																Buttons
															</span>
														</a>
													</li>
													<li class="m-menu__item " aria-haspopup="true" >
														<a  href="crud/datatables/extensions/colreorder.html" class="m-menu__link ">
															<i class="m-menu__link-bullet m-menu__link-bullet--dot">
															<span></span>
															</i>
															<span class="m-menu__link-text">
																ColReorder
															</span>
														</a>
													</li>
													<li class="m-menu__item " aria-haspopup="true" >
														<a  href="crud/datatables/extensions/keytable.html" class="m-menu__link ">
															<i class="m-menu__link-bullet m-menu__link-bullet--dot">
															<span></span>
															</i>
															<span class="m-menu__link-text">
																KeyTable
															</span>
														</a>
													</li>
													<li class="m-menu__item " aria-haspopup="true" >
														<a  href="crud/datatables/extensions/responsive.html" class="m-menu__link ">
															<i class="m-menu__link-bullet m-menu__link-bullet--dot">
															<span></span>
															</i>
															<span class="m-menu__link-text">
																Responsive
															</span>
														</a>
													</li>
													<li class="m-menu__item " aria-haspopup="true" >
														<a  href="crud/datatables/extensions/rowgroup.html" class="m-menu__link ">
															<i class="m-menu__link-bullet m-menu__link-bullet--dot">
															<span></span>
															</i>
															<span class="m-menu__link-text">
																RowGroup
															</span>
														</a>
													</li>
													<li class="m-menu__item " aria-haspopup="true" >
														<a  href="crud/datatables/extensions/rowreorder.html" class="m-menu__link ">
															<i class="m-menu__link-bullet m-menu__link-bullet--dot">
															<span></span>
															</i>
															<span class="m-menu__link-text">
																RowReorder
															</span>
														</a>
													</li>
													<li class="m-menu__item " aria-haspopup="true" >
														<a  href="crud/datatables/extensions/scroller.html" class="m-menu__link ">
															<i class="m-menu__link-bullet m-menu__link-bullet--dot">
															<span></span>
															</i>
															<span class="m-menu__link-text">
																Scroller
															</span>
														</a>
													</li>
													<li class="m-menu__item " aria-haspopup="true" >
														<a  href="crud/datatables/extensions/select.html" class="m-menu__link ">
															<i class="m-menu__link-bullet m-menu__link-bullet--dot">
															<span></span>
															</i>
															<span class="m-menu__link-text">
																Select
															</span>
														</a>
													</li>
												</ul>
											</div>
										</li>
									</ul>
								</div>
							</li>
							<li class="m-menu__section ">
								<h4 class="m-menu__section-text">
								Snippets
								</h4>
								<i class="m-menu__section-icon flaticon-more-v3"></i>
							</li>
							<li class="m-menu__item  m-menu__item--submenu" aria-haspopup="true"  m-menu-submenu-toggle="hover">
								<a  href="javascript:;" class="m-menu__link m-menu__toggle">
									<i class="m-menu__link-icon flaticon-interface-9"></i>
									<span class="m-menu__link-text">
										Pricing Tables
									</span>
									<i class="m-menu__ver-arrow la la-angle-right"></i>
								</a>
								<div class="m-menu__submenu ">
									<span class="m-menu__arrow"></span>
									<ul class="m-menu__subnav">
										<li class="m-menu__item  m-menu__item--parent" aria-haspopup="true" >
											<span class="m-menu__link">
												<span class="m-menu__link-text">
													Pricing Tables
												</span>
											</span>
										</li>
										<li class="m-menu__item " aria-haspopup="true" >
											<a  href="snippets/general/pricing-tables/pricing-table-1.html" class="m-menu__link ">
												<i class="m-menu__link-bullet m-menu__link-bullet--dot">
												<span></span>
												</i>
												<span class="m-menu__link-text">
													Pricing Tables v1
												</span>
											</a>
										</li>
										<li class="m-menu__item " aria-haspopup="true" >
											<a  href="snippets/general/pricing-tables/pricing-table-2.html" class="m-menu__link ">
												<i class="m-menu__link-bullet m-menu__link-bullet--dot">
												<span></span>
												</i>
												<span class="m-menu__link-text">
													Pricing Tables v2
												</span>
											</a>
										</li>
										<li class="m-menu__item " aria-haspopup="true" >
											<a  href="snippets/general/pricing-tables/pricing-table-3.html" class="m-menu__link ">
												<i class="m-menu__link-bullet m-menu__link-bullet--dot">
												<span></span>
												</i>
												<span class="m-menu__link-text">
													Pricing Tables v3
												</span>
											</a>
										</li>
										<li class="m-menu__item " aria-haspopup="true" >
											<a  href="snippets/general/pricing-tables/pricing-table-4.html" class="m-menu__link ">
												<i class="m-menu__link-bullet m-menu__link-bullet--dot">
												<span></span>
												</i>
												<span class="m-menu__link-text">
													Pricing Tables v4
												</span>
											</a>
										</li>
									</ul>
								</div>
							</li>
							<li class="m-menu__item  m-menu__item--submenu" aria-haspopup="true"  m-menu-submenu-toggle="hover">
								<a  href="javascript:;" class="m-menu__link m-menu__toggle">
									<i class="m-menu__link-icon flaticon-piggy-bank"></i>
									<span class="m-menu__link-text">
										Invoices
									</span>
									<i class="m-menu__ver-arrow la la-angle-right"></i>
								</a>
								<div class="m-menu__submenu ">
									<span class="m-menu__arrow"></span>
									<ul class="m-menu__subnav">
										<li class="m-menu__item  m-menu__item--parent" aria-haspopup="true" >
											<span class="m-menu__link">
												<span class="m-menu__link-text">
													Invoices
												</span>
											</span>
										</li>
										<li class="m-menu__item " aria-haspopup="true" >
											<a  href="snippets/invoices/invoice-1.html" class="m-menu__link ">
												<i class="m-menu__link-bullet m-menu__link-bullet--dot">
												<span></span>
												</i>
												<span class="m-menu__link-text">
													Invoice v1
												</span>
											</a>
										</li>
										<li class="m-menu__item " aria-haspopup="true" >
											<a  href="snippets/invoices/invoice-2.html" class="m-menu__link ">
												<i class="m-menu__link-bullet m-menu__link-bullet--dot">
												<span></span>
												</i>
												<span class="m-menu__link-text">
													Invoice v2
												</span>
											</a>
										</li>
									</ul>
								</div>
							</li>
							<li class="m-menu__item  m-menu__item--submenu" aria-haspopup="true"  m-menu-submenu-toggle="hover">
								<a  href="javascript:;" class="m-menu__link m-menu__toggle">
									<i class="m-menu__link-icon flaticon-exclamation"></i>
									<span class="m-menu__link-text">
										FAQS
									</span>
									<i class="m-menu__ver-arrow la la-angle-right"></i>
								</a>
								<div class="m-menu__submenu ">
									<span class="m-menu__arrow"></span>
									<ul class="m-menu__subnav">
										<li class="m-menu__item  m-menu__item--parent" aria-haspopup="true" >
											<span class="m-menu__link">
												<span class="m-menu__link-text">
													FAQS
												</span>
											</span>
										</li>
										<li class="m-menu__item " aria-haspopup="true" >
											<a  href="snippets/faq/faq-1.html" class="m-menu__link ">
												<i class="m-menu__link-bullet m-menu__link-bullet--dot">
												<span></span>
												</i>
												<span class="m-menu__link-text">
													FAQ v1
												</span>
											</a>
										</li>
									</ul>
								</div>
							</li>
							<li class="m-menu__item  m-menu__item--submenu" aria-haspopup="true"  m-menu-submenu-toggle="hover">
								<a  href="javascript:;" class="m-menu__link m-menu__toggle">
									<i class="m-menu__link-icon flaticon-suitcase"></i>
									<span class="m-menu__link-text">
										Custom Pages
									</span>
									<i class="m-menu__ver-arrow la la-angle-right"></i>
								</a>
								<div class="m-menu__submenu ">
									<span class="m-menu__arrow"></span>
									<ul class="m-menu__subnav">
										<li class="m-menu__item  m-menu__item--parent" aria-haspopup="true" >
											<span class="m-menu__link">
												<span class="m-menu__link-text">
													Custom Pages
												</span>
											</span>
										</li>
										<li class="m-menu__item  m-menu__item--submenu" aria-haspopup="true"  m-menu-submenu-toggle="hover">
											<a  href="javascript:;" class="m-menu__link m-menu__toggle">
												<i class="m-menu__link-bullet m-menu__link-bullet--dot">
												<span></span>
												</i>
												<span class="m-menu__link-text">
													User Pages
												</span>
												<i class="m-menu__ver-arrow la la-angle-right"></i>
											</a>
											<div class="m-menu__submenu ">
												<span class="m-menu__arrow"></span>
												<ul class="m-menu__subnav">
													<li class="m-menu__item " aria-haspopup="true" >
														<a target="_blank" href="snippets/pages/user/login-1.html" class="m-menu__link ">
															<i class="m-menu__link-bullet m-menu__link-bullet--dot">
															<span></span>
															</i>
															<span class="m-menu__link-text">
																Login - 1
															</span>
														</a>
													</li>
													<li class="m-menu__item " aria-haspopup="true" >
														<a target="_blank" href="snippets/pages/user/login-2.html" class="m-menu__link ">
															<i class="m-menu__link-bullet m-menu__link-bullet--dot">
															<span></span>
															</i>
															<span class="m-menu__link-text">
																Login - 2
															</span>
														</a>
													</li>
													<li class="m-menu__item " aria-haspopup="true" >
														<a target="_blank" href="snippets/pages/user/login-3.html" class="m-menu__link ">
															<i class="m-menu__link-bullet m-menu__link-bullet--dot">
															<span></span>
															</i>
															<span class="m-menu__link-text">
																Login - 3
															</span>
														</a>
													</li>
													<li class="m-menu__item " aria-haspopup="true" >
														<a target="_blank" href="snippets/pages/user/login-4.html" class="m-menu__link ">
															<i class="m-menu__link-bullet m-menu__link-bullet--dot">
															<span></span>
															</i>
															<span class="m-menu__link-text">
																Login - 4
															</span>
														</a>
													</li>
													<li class="m-menu__item " aria-haspopup="true" >
														<a target="_blank" href="snippets/pages/user/login-5.html" class="m-menu__link ">
															<i class="m-menu__link-bullet m-menu__link-bullet--dot">
															<span></span>
															</i>
															<span class="m-menu__link-text">
																Login - 5
															</span>
														</a>
													</li>
												</ul>
											</div>
										</li>
										<li class="m-menu__item  m-menu__item--submenu" aria-haspopup="true"  m-menu-submenu-toggle="hover">
											<a  href="javascript:;" class="m-menu__link m-menu__toggle">
												<i class="m-menu__link-bullet m-menu__link-bullet--dot">
												<span></span>
												</i>
												<span class="m-menu__link-text">
													Error Pages
												</span>
												<i class="m-menu__ver-arrow la la-angle-right"></i>
											</a>
											<div class="m-menu__submenu ">
												<span class="m-menu__arrow"></span>
												<ul class="m-menu__subnav">
													<li class="m-menu__item " aria-haspopup="true" >
														<a target="_blank" href="snippets/pages/errors/error-1.html" class="m-menu__link ">
															<i class="m-menu__link-bullet m-menu__link-bullet--dot">
															<span></span>
															</i>
															<span class="m-menu__link-text">
																Error 1
															</span>
														</a>
													</li>
													<li class="m-menu__item " aria-haspopup="true" >
														<a target="_blank" href="snippets/pages/errors/error-2.html" class="m-menu__link ">
															<i class="m-menu__link-bullet m-menu__link-bullet--dot">
															<span></span>
															</i>
															<span class="m-menu__link-text">
																Error 2
															</span>
														</a>
													</li>
													<li class="m-menu__item " aria-haspopup="true" >
														<a target="_blank" href="snippets/pages/errors/error-3.html" class="m-menu__link ">
															<i class="m-menu__link-bullet m-menu__link-bullet--dot">
															<span></span>
															</i>
															<span class="m-menu__link-text">
																Error 3
															</span>
														</a>
													</li>
													<li class="m-menu__item " aria-haspopup="true" >
														<a target="_blank" href="snippets/pages/errors/error-4.html" class="m-menu__link ">
															<i class="m-menu__link-bullet m-menu__link-bullet--dot">
															<span></span>
															</i>
															<span class="m-menu__link-text">
																Error 4
															</span>
														</a>
													</li>
													<li class="m-menu__item " aria-haspopup="true" >
														<a target="_blank" href="snippets/pages/errors/error-5.html" class="m-menu__link ">
															<i class="m-menu__link-bullet m-menu__link-bullet--dot">
															<span></span>
															</i>
															<span class="m-menu__link-text">
																Error 5
															</span>
														</a>
													</li>
													<li class="m-menu__item " aria-haspopup="true" >
														<a target="_blank" href="snippets/pages/errors/error-6.html" class="m-menu__link ">
															<i class="m-menu__link-bullet m-menu__link-bullet--dot">
															<span></span>
															</i>
															<span class="m-menu__link-text">
																Error 6
															</span>
														</a>
													</li>
												</ul>
											</div>
										</li>
									</ul>
								</div>
							</li>
						</ul>
					</div>
					<!-- END: Aside Menu -->
				</div>
				<!-- END: Left Aside -->
				<div class="m-grid__item m-grid__item--fluid m-wrapper">
					<div class="m-subheader ">
						<div class="d-flex align-items-center">
							<div class="mr-auto">
								<ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
									<li class="m-nav__item m-nav__item--home">
										<a href="{{ url('/') }}" class="m-nav__link m-nav__link--icon">
											<i class="m-nav__link-icon la la-home"></i>
										</a>
									</li>
									<li class="m-nav__item">
										<a href="" class="m-nav__link">
											<span class="m-nav__link-text">
												{{ Route::currentRouteName() }}
											</span>
										</a>
									</li>
								</ul>
							</div>
						</div>
					</div>
					@yield('content')
				</div>
			</div>
			<!-- end:: Body -->
			<!-- begin::Footer -->
			<footer class="m-grid__item	m-footer ">
				<div class="m-container m-container--fluid m-container--full-height m-page__container">
					<div class="m-stack m-stack--flex-tablet-and-mobile m-stack--ver m-stack--desktop">
						<div class="m-stack__item m-stack__item--left m-stack__item--middle m-stack__item--last">
							<span class="m-footer__copyright">
								{{ date('Y') }} &copy;
								<a href="{{ url('/') }}" class="m-link">
									{{ config('app.name', 'Laravel') }}
								</a>
							</span>
						</div>
					</div>
				</div>
			</footer>
			<!-- end::Footer -->
		</div>
		<!-- end:: Page -->
		<!-- begin::Quick Sidebar -->
		<div id="m_quick_sidebar" class="m-quick-sidebar m-quick-sidebar--tabbed m-quick-sidebar--skin-light">
			<div class="m-quick-sidebar__content m--hide">
				<span id="m_quick_sidebar_close" class="m-quick-sidebar__close">
					<i class="la la-close"></i>
				</span>
				<ul id="m_quick_sidebar_tabs" class="nav nav-tabs m-tabs m-tabs-line m-tabs-line--brand" role="tablist">
					<li class="nav-item m-tabs__item">
						<a class="nav-link m-tabs__link active" data-toggle="tab" href="#m_quick_sidebar_tabs_messenger" role="tab">
							Messages
						</a>
					</li>
					<li class="nav-item m-tabs__item">
						<a class="nav-link m-tabs__link" 		data-toggle="tab" href="#m_quick_sidebar_tabs_settings" role="tab">
							Settings
						</a>
					</li>
					<li class="nav-item m-tabs__item">
						<a class="nav-link m-tabs__link" data-toggle="tab" href="#m_quick_sidebar_tabs_logs" role="tab">
							Logs
						</a>
					</li>
				</ul>
				<div class="tab-content">
					<div class="tab-pane active m-scrollable" id="m_quick_sidebar_tabs_messenger" role="tabpanel">
						<div class="m-messenger m-messenger--message-arrow m-messenger--skin-light">
							<div class="m-messenger__messages">
								<div class="m-messenger__wrapper">
									<div class="m-messenger__message m-messenger__message--in">
										<div class="m-messenger__message-pic">
											<img src="{{ asset('assets/app/media/img//users/user3.jpg') }}" alt=""/>
										</div>
										<div class="m-messenger__message-body">
											<div class="m-messenger__message-arrow"></div>
											<div class="m-messenger__message-content">
												<div class="m-messenger__message-username">
													Megan wrote
												</div>
												<div class="m-messenger__message-text">
													Hi Bob. What time will be the meeting ?
												</div>
											</div>
										</div>
									</div>
								</div>
								<div class="m-messenger__wrapper">
									<div class="m-messenger__message m-messenger__message--out">
										<div class="m-messenger__message-body">
											<div class="m-messenger__message-arrow"></div>
											<div class="m-messenger__message-content">
												<div class="m-messenger__message-text">
													Hi Megan. It's at 2.30PM
												</div>
											</div>
										</div>
									</div>
								</div>
								<div class="m-messenger__wrapper">
									<div class="m-messenger__message m-messenger__message--in">
										<div class="m-messenger__message-pic">
											<img src="{{ asset('assets/app/media/img//users/user3.jpg') }}" alt=""/>
										</div>
										<div class="m-messenger__message-body">
											<div class="m-messenger__message-arrow"></div>
											<div class="m-messenger__message-content">
												<div class="m-messenger__message-username">
													Megan wrote
												</div>
												<div class="m-messenger__message-text">
													Will the development team be joining ?
												</div>
											</div>
										</div>
									</div>
								</div>
								<div class="m-messenger__wrapper">
									<div class="m-messenger__message m-messenger__message--out">
										<div class="m-messenger__message-body">
											<div class="m-messenger__message-arrow"></div>
											<div class="m-messenger__message-content">
												<div class="m-messenger__message-text">
													Yes sure. I invited them as well
												</div>
											</div>
										</div>
									</div>
								</div>
								<div class="m-messenger__datetime">
									2:30PM
								</div>
								<div class="m-messenger__wrapper">
									<div class="m-messenger__message m-messenger__message--in">
										<div class="m-messenger__message-pic">
											<img src="{{ asset('assets/app/media/img//users/user3.jpg') }}"  alt=""/>
										</div>
										<div class="m-messenger__message-body">
											<div class="m-messenger__message-arrow"></div>
											<div class="m-messenger__message-content">
												<div class="m-messenger__message-username">
													Megan wrote
												</div>
												<div class="m-messenger__message-text">
													Noted. For the Coca-Cola Mobile App project as well ?
												</div>
											</div>
										</div>
									</div>
								</div>
								<div class="m-messenger__wrapper">
									<div class="m-messenger__message m-messenger__message--out">
										<div class="m-messenger__message-body">
											<div class="m-messenger__message-arrow"></div>
											<div class="m-messenger__message-content">
												<div class="m-messenger__message-text">
													Yes, sure.
												</div>
											</div>
										</div>
									</div>
								</div>
								<div class="m-messenger__wrapper">
									<div class="m-messenger__message m-messenger__message--out">
										<div class="m-messenger__message-body">
											<div class="m-messenger__message-arrow"></div>
											<div class="m-messenger__message-content">
												<div class="m-messenger__message-text">
													Please also prepare the quotation for the Loop CRM project as well.
												</div>
											</div>
										</div>
									</div>
								</div>
								<div class="m-messenger__datetime">
									3:15PM
								</div>
								<div class="m-messenger__wrapper">
									<div class="m-messenger__message m-messenger__message--in">
										<div class="m-messenger__message-no-pic m--bg-fill-danger">
											<span>
												M
											</span>
										</div>
										<div class="m-messenger__message-body">
											<div class="m-messenger__message-arrow"></div>
											<div class="m-messenger__message-content">
												<div class="m-messenger__message-username">
													Megan wrote
												</div>
												<div class="m-messenger__message-text">
													Noted. I will prepare it.
												</div>
											</div>
										</div>
									</div>
								</div>
								<div class="m-messenger__wrapper">
									<div class="m-messenger__message m-messenger__message--out">
										<div class="m-messenger__message-body">
											<div class="m-messenger__message-arrow"></div>
											<div class="m-messenger__message-content">
												<div class="m-messenger__message-text">
													Thanks Megan. I will see you later.
												</div>
											</div>
										</div>
									</div>
								</div>
								<div class="m-messenger__wrapper">
									<div class="m-messenger__message m-messenger__message--in">
										<div class="m-messenger__message-pic">
											<img src="{{ asset('assets/app/media/img//users/user3.jpg') }}"  alt=""/>
										</div>
										<div class="m-messenger__message-body">
											<div class="m-messenger__message-arrow"></div>
											<div class="m-messenger__message-content">
												<div class="m-messenger__message-username">
													Megan wrote
												</div>
												<div class="m-messenger__message-text">
													Sure. See you in the meeting soon.
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="m-messenger__seperator"></div>
							<div class="m-messenger__form">
								<div class="m-messenger__form-controls">
									<input type="text" name="" placeholder="Type here..." class="m-messenger__form-input">
								</div>
								<div class="m-messenger__form-tools">
									<a href="" class="m-messenger__form-attachment">
										<i class="la la-paperclip"></i>
									</a>
								</div>
							</div>
						</div>
					</div>
					<div class="tab-pane  m-scrollable" id="m_quick_sidebar_tabs_settings" role="tabpanel">
						<div class="m-list-settings">
							<div class="m-list-settings__group">
								<div class="m-list-settings__heading">
									General Settings
								</div>
								<div class="m-list-settings__item">
									<span class="m-list-settings__item-label">
										Email Notifications
									</span>
									<span class="m-list-settings__item-control">
										<span class="m-switch m-switch--outline m-switch--icon-check m-switch--brand">
											<label>
												<input type="checkbox" checked="checked" name="">
												<span></span>
											</label>
										</span>
									</span>
								</div>
								<div class="m-list-settings__item">
									<span class="m-list-settings__item-label">
										Site Tracking
									</span>
									<span class="m-list-settings__item-control">
										<span class="m-switch m-switch--outline m-switch--icon-check m-switch--brand">
											<label>
												<input type="checkbox" name="">
												<span></span>
											</label>
										</span>
									</span>
								</div>
								<div class="m-list-settings__item">
									<span class="m-list-settings__item-label">
										SMS Alerts
									</span>
									<span class="m-list-settings__item-control">
										<span class="m-switch m-switch--outline m-switch--icon-check m-switch--brand">
											<label>
												<input type="checkbox" name="">
												<span></span>
											</label>
										</span>
									</span>
								</div>
								<div class="m-list-settings__item">
									<span class="m-list-settings__item-label">
										Backup Storage
									</span>
									<span class="m-list-settings__item-control">
										<span class="m-switch m-switch--outline m-switch--icon-check m-switch--brand">
											<label>
												<input type="checkbox" name="">
												<span></span>
											</label>
										</span>
									</span>
								</div>
								<div class="m-list-settings__item">
									<span class="m-list-settings__item-label">
										Audit Logs
									</span>
									<span class="m-list-settings__item-control">
										<span class="m-switch m-switch--outline m-switch--icon-check m-switch--brand">
											<label>
												<input type="checkbox" checked="checked" name="">
												<span></span>
											</label>
										</span>
									</span>
								</div>
							</div>
							<div class="m-list-settings__group">
								<div class="m-list-settings__heading">
									System Settings
								</div>
								<div class="m-list-settings__item">
									<span class="m-list-settings__item-label">
										System Logs
									</span>
									<span class="m-list-settings__item-control">
										<span class="m-switch m-switch--outline m-switch--icon-check m-switch--brand">
											<label>
												<input type="checkbox" name="">
												<span></span>
											</label>
										</span>
									</span>
								</div>
								<div class="m-list-settings__item">
									<span class="m-list-settings__item-label">
										Error Reporting
									</span>
									<span class="m-list-settings__item-control">
										<span class="m-switch m-switch--outline m-switch--icon-check m-switch--brand">
											<label>
												<input type="checkbox" name="">
												<span></span>
											</label>
										</span>
									</span>
								</div>
								<div class="m-list-settings__item">
									<span class="m-list-settings__item-label">
										Applications Logs
									</span>
									<span class="m-list-settings__item-control">
										<span class="m-switch m-switch--outline m-switch--icon-check m-switch--brand">
											<label>
												<input type="checkbox" name="">
												<span></span>
											</label>
										</span>
									</span>
								</div>
								<div class="m-list-settings__item">
									<span class="m-list-settings__item-label">
										Backup Servers
									</span>
									<span class="m-list-settings__item-control">
										<span class="m-switch m-switch--outline m-switch--icon-check m-switch--brand">
											<label>
												<input type="checkbox" checked="checked" name="">
												<span></span>
											</label>
										</span>
									</span>
								</div>
								<div class="m-list-settings__item">
									<span class="m-list-settings__item-label">
										Audit Logs
									</span>
									<span class="m-list-settings__item-control">
										<span class="m-switch m-switch--outline m-switch--icon-check m-switch--brand">
											<label>
												<input type="checkbox" name="">
												<span></span>
											</label>
										</span>
									</span>
								</div>
							</div>
						</div>
					</div>
					<div class="tab-pane  m-scrollable" id="m_quick_sidebar_tabs_logs" role="tabpanel">
						<div class="m-list-timeline">
							<div class="m-list-timeline__group">
								<div class="m-list-timeline__heading">
									System Logs
								</div>
								<div class="m-list-timeline__items">
									<div class="m-list-timeline__item">
										<span class="m-list-timeline__badge m-list-timeline__badge--state-success"></span>
										<a href="" class="m-list-timeline__text">
											12 new users registered
											<span class="m-badge m-badge--warning m-badge--wide">
												important
											</span>
										</a>
										<span class="m-list-timeline__time">
											Just now
										</span>
									</div>
									<div class="m-list-timeline__item">
										<span class="m-list-timeline__badge m-list-timeline__badge--state-info"></span>
										<a href="" class="m-list-timeline__text">
											System shutdown
										</a>
										<span class="m-list-timeline__time">
											11 mins
										</span>
									</div>
									<div class="m-list-timeline__item">
										<span class="m-list-timeline__badge m-list-timeline__badge--state-danger"></span>
										<a href="" class="m-list-timeline__text">
											New invoice received
										</a>
										<span class="m-list-timeline__time">
											20 mins
										</span>
									</div>
									<div class="m-list-timeline__item">
										<span class="m-list-timeline__badge m-list-timeline__badge--state-warning"></span>
										<a href="" class="m-list-timeline__text">
											Database overloaded 89%
											<span class="m-badge m-badge--success m-badge--wide">
												resolved
											</span>
										</a>
										<span class="m-list-timeline__time">
											1 hr
										</span>
									</div>
									<div class="m-list-timeline__item">
										<span class="m-list-timeline__badge m-list-timeline__badge--state-success"></span>
										<a href="" class="m-list-timeline__text">
											System error
										</a>
										<span class="m-list-timeline__time">
											2 hrs
										</span>
									</div>
									<div class="m-list-timeline__item">
										<span class="m-list-timeline__badge m-list-timeline__badge--state-info"></span>
										<a href="" class="m-list-timeline__text">
											Production server down
											<span class="m-badge m-badge--danger m-badge--wide">
												pending
											</span>
										</a>
										<span class="m-list-timeline__time">
											3 hrs
										</span>
									</div>
									<div class="m-list-timeline__item">
										<span class="m-list-timeline__badge m-list-timeline__badge--state-success"></span>
										<a href="" class="m-list-timeline__text">
											Production server up
										</a>
										<span class="m-list-timeline__time">
											5 hrs
										</span>
									</div>
								</div>
							</div>
							<div class="m-list-timeline__group">
								<div class="m-list-timeline__heading">
									Applications Logs
								</div>
								<div class="m-list-timeline__items">
									<div class="m-list-timeline__item">
										<span class="m-list-timeline__badge m-list-timeline__badge--state-info"></span>
										<a href="" class="m-list-timeline__text">
											New order received
											<span class="m-badge m-badge--info m-badge--wide">
												urgent
											</span>
										</a>
										<span class="m-list-timeline__time">
											7 hrs
										</span>
									</div>
									<div class="m-list-timeline__item">
										<span class="m-list-timeline__badge m-list-timeline__badge--state-success"></span>
										<a href="" class="m-list-timeline__text">
											12 new users registered
										</a>
										<span class="m-list-timeline__time">
											Just now
										</span>
									</div>
									<div class="m-list-timeline__item">
										<span class="m-list-timeline__badge m-list-timeline__badge--state-info"></span>
										<a href="" class="m-list-timeline__text">
											System shutdown
										</a>
										<span class="m-list-timeline__time">
											11 mins
										</span>
									</div>
									<div class="m-list-timeline__item">
										<span class="m-list-timeline__badge m-list-timeline__badge--state-danger"></span>
										<a href="" class="m-list-timeline__text">
											New invoices received
										</a>
										<span class="m-list-timeline__time">
											20 mins
										</span>
									</div>
									<div class="m-list-timeline__item">
										<span class="m-list-timeline__badge m-list-timeline__badge--state-warning"></span>
										<a href="" class="m-list-timeline__text">
											Database overloaded 89%
										</a>
										<span class="m-list-timeline__time">
											1 hr
										</span>
									</div>
									<div class="m-list-timeline__item">
										<span class="m-list-timeline__badge m-list-timeline__badge--state-success"></span>
										<a href="" class="m-list-timeline__text">
											System error
											<span class="m-badge m-badge--info m-badge--wide">
												pending
											</span>
										</a>
										<span class="m-list-timeline__time">
											2 hrs
										</span>
									</div>
									<div class="m-list-timeline__item">
										<span class="m-list-timeline__badge m-list-timeline__badge--state-info"></span>
										<a href="" class="m-list-timeline__text">
											Production server down
										</a>
										<span class="m-list-timeline__time">
											3 hrs
										</span>
									</div>
								</div>
							</div>
							<div class="m-list-timeline__group">
								<div class="m-list-timeline__heading">
									Server Logs
								</div>
								<div class="m-list-timeline__items">
									<div class="m-list-timeline__item">
										<span class="m-list-timeline__badge m-list-timeline__badge--state-success"></span>
										<a href="" class="m-list-timeline__text">
											Production server up
										</a>
										<span class="m-list-timeline__time">
											5 hrs
										</span>
									</div>
									<div class="m-list-timeline__item">
										<span class="m-list-timeline__badge m-list-timeline__badge--state-info"></span>
										<a href="" class="m-list-timeline__text">
											New order received
										</a>
										<span class="m-list-timeline__time">
											7 hrs
										</span>
									</div>
									<div class="m-list-timeline__item">
										<span class="m-list-timeline__badge m-list-timeline__badge--state-success"></span>
										<a href="" class="m-list-timeline__text">
											12 new users registered
										</a>
										<span class="m-list-timeline__time">
											Just now
										</span>
									</div>
									<div class="m-list-timeline__item">
										<span class="m-list-timeline__badge m-list-timeline__badge--state-info"></span>
										<a href="" class="m-list-timeline__text">
											System shutdown
										</a>
										<span class="m-list-timeline__time">
											11 mins
										</span>
									</div>
									<div class="m-list-timeline__item">
										<span class="m-list-timeline__badge m-list-timeline__badge--state-danger"></span>
										<a href="" class="m-list-timeline__text">
											New invoice received
										</a>
										<span class="m-list-timeline__time">
											20 mins
										</span>
									</div>
									<div class="m-list-timeline__item">
										<span class="m-list-timeline__badge m-list-timeline__badge--state-warning"></span>
										<a href="" class="m-list-timeline__text">
											Database overloaded 89%
										</a>
										<span class="m-list-timeline__time">
											1 hr
										</span>
									</div>
									<div class="m-list-timeline__item">
										<span class="m-list-timeline__badge m-list-timeline__badge--state-success"></span>
										<a href="" class="m-list-timeline__text">
											System error
										</a>
										<span class="m-list-timeline__time">
											2 hrs
										</span>
									</div>
									<div class="m-list-timeline__item">
										<span class="m-list-timeline__badge m-list-timeline__badge--state-info"></span>
										<a href="" class="m-list-timeline__text">
											Production server down
										</a>
										<span class="m-list-timeline__time">
											3 hrs
										</span>
									</div>
									<div class="m-list-timeline__item">
										<span class="m-list-timeline__badge m-list-timeline__badge--state-success"></span>
										<a href="" class="m-list-timeline__text">
											Production server up
										</a>
										<span class="m-list-timeline__time">
											5 hrs
										</span>
									</div>
									<div class="m-list-timeline__item">
										<span class="m-list-timeline__badge m-list-timeline__badge--state-info"></span>
										<a href="" class="m-list-timeline__text">
											New order received
										</a>
										<span class="m-list-timeline__time">
											1117 hrs
										</span>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- end::Quick Sidebar -->
		<!-- begin::Scroll Top -->
		<div id="m_scroll_top" class="m-scroll-top">
			<i class="la la-arrow-up"></i>
		</div>
		<!-- end::Scroll Top -->
		<!--begin::Base Scripts -->
		<script src="{{ asset('assets/vendors/base/vendors.bundle.js') }}" type="text/javascript"></script>
		<script src="{{ asset('assets/demo/default/base/scripts.bundle.js') }}" type="text/javascript"></script>
		<!--end::Base Scripts -->
		<!--begin::Page Vendors -->
		<script src="{{ asset('assets/vendors/custom/fullcalendar/fullcalendar.bundle.js') }}" type="text/javascript"></script>
		<!--end::Page Vendors -->
		<!--begin::Page Snippets -->
		<script src="{{ asset('assets/app/js/dashboard.js') }}" type="text/javascript"></script>
		<!--end::Page Snippets -->
        <!--begin::Page Resources -->
		<script src="{{ asset('assets/demo/default/custom/crud/metronic-datatable/base/html-table.js') }}" type="text/javascript"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.5.7/jquery.fancybox.min.js" type="text/javascript" ></script>
		<script src="{{ asset('assets/demo/default/custom/crud/forms/widgets/select2.js') }}" type="text/javascript"></script>
		<script src="{{ asset('assets/demo/default/custom/crud/forms/widgets/nouislider.js') }}" type="text/javascript"></script>
		<script src="{{ asset('assets/demo/default/custom/crud/forms/validation/form-controls.js') }}" type="text/javascript"></script>
		<script src="{{ asset('assets/vendors/custom/flot/flot.bundle.js') }}" type="text/javascript"></script>
		<script src="{{ asset('assets/demo/default/custom/components/charts/flotcharts.js') }}" type="text/javascript"></script>
		<script src="{{ asset('assets/demo/default/custom/crud/forms/widgets/ion-range-slider.js') }}" type="text/javascript"></script>
		<script src="{{ asset('assets/demo/default/custom/components/base/toastr.js') }}" type="text/javascript"></script>
		<script type="text/javascript">
			$(document).ready(function(){
			  	$("#showCart").click(function() {
					
			    	$.ajax({
			    		type: 'GET',
			    		url: '{{ route('order.show_cart') }}', 
			    		async: true, 
			    		success: function(data, result,status,xhr) {
							console.log(data);
							$('#cart-total').text(data.data.length);
							$('#cart-notifications').text('Menunggu Konfirmasi');
							data.data.forEach(function(value) {
								$("#menunggu-keputusan").append('<div class="m-list-timeline__item"><span class="m-list-timeline__badge"></span><span class="m-list-timeline__text"> Serial Number : '+value.sn+'<br> Brand : '+value.brand_name+'<br> Model : '+value.model_name+'<br> Sku : '+value.sku_name+'<br> Grade : '+value.grade_name+'<br> Price : '+value.price+'<br><span class="m-badge m-badge--success m-badge--wide">pending</span></span></div>');
							});
			    	    },
			    	    error: function(xhr,status,error) {
			    	    	console.log(xhr,status,error);
			    	    }
			    	});
			  	});
			});

			function clear() {
				$('#cart-total');
				$('#cart-notifications');
				$("#menunggu-keputusan");
			}
		</script>
	</body>
	<!-- end::Body -->
</html>