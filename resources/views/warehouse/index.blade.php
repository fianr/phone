@extends('layouts.index')
@section('content')
<div class="m-content">
	<div class="m-portlet m-portlet--head-sm m-portlet--collapse" m-portlet="true">
		<div class="m-portlet__head">
			<div class="m-portlet__head-caption">
				<div class="m-portlet__head-title">
					<span class="m-portlet__head-icon">
						<i class="flaticon-search"></i>
					</span>
					<h3 class="m-portlet__head-text">
					Search Tools
					</h3>
				</div>
			</div>
			<div class="m-portlet__head-tools">
				<ul class="m-portlet__nav">
					<li class="m-portlet__nav-item">
						<a href=""  m-portlet-tool="toggle" class="m-portlet__nav-link m-portlet__nav-link--icon">
							<i class="la la-angle-down"></i>
						</a>
					</li>
				</ul>
			</div>
		</div>
		<!--begin::Form-->
		<!--end::Form-->
	</div>
	<div class="m-portlet m-portlet--mobile">
		<div class="m-portlet__head">
			<div class="m-portlet__head-caption">
				<div class="m-portlet__head-title">
					<h3 class="m-portlet__head-text">
					Warehouse List
					</h3>
				</div>
			</div>
		</div>
		<div class="m-portlet__body">
			<!--begin: Datatable -->
			<table class="m-datatable" id="html_table" width="100%">
				<thead>
					<tr>
						<th data-field="no">
							No
						</th>
						<th data-field="sn">
							Serial Number
						</th>
						<th>
							Brand
						</th>
						<th>
							Name
						</th>
						<th>
							Sku Name
						</th>
						<th>
							Price
						</th>
						<th>
							Grade
						</th>
						<th>
							Model Picture
						</th>
						<th>
							Status
						</th>
						<th>
							Date
						</th>
						<th>
							Action
						</th>
					</tr>
				</thead>
				<tbody>
					@foreach($qcBill as $value)
						<tr>
							<td>{{ ++$i }}</td>
							<td>{{ $value->fitemSn }}</td>
							<td>{{ $value->ItemBase->brand->fname }}</td>
							<td>{{ $value->fitembaseName }}</td>
							<td>{{ $value->fitemSkuName }}</td>
							<td>{{ ($value->Order->fdeal_amount != 0) ? number_format($value->Order->fdeal_amount, 0) : number_format($value->Order->fcash_amount, 0) }}</td>
							<td>{{ $value->fqcGradeName }}</td>
							<td>
								@foreach($value->order->orderFile->where('ftype', 2) as $orderfile)
									<a href="{{ 'https://ossapi.joyexchange.id/'.$orderfile->fpath }}" data-fancybox="images" data-caption="Serial Number : {{ $value->fitemSn }}
										&lt;/b&gt;&lt;br /&gt;Name : {{ $value->fitembaseName }}&lt;/b&gt;&lt;br /&gt;Sku Name : {{ $value->fitemSkuName }}
										&lt;/b&gt;&lt;br /&gt;Price : {{ ($value->Order->fdeal_amount != 0) ? number_format($value->Order->fdeal_amount, 0) : number_format($value->Order->fcash_amount, 0) }}
										&lt;/b&gt;&lt;br /&gt;Grade : {{ ($value->fqcGradeName) }}">
										<img src="{{ 'https://ossapi.joyexchange.id/'.$orderfile->fpath }}" width="50" height="50">
									</a>
								@endforeach
							</td>
							<td>{{ $value->statusName[$value->fstatus] }}</td>
							<td>{{ date('Y-m-d', $value->fbillTime) }}</td>
							<td>
								<a onclick="view(this)" data-id="{{ $value->fid }}" data-toggle="modal" data-target="#" class="m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill" title="View">
									<i class="la la-eye"></i>
								</a>
							</td>
						</tr>
					@endforeach
				</tbody>
			</table>
			{{ $qcBill->links() }}
			<!--end: Datatable -->
		</div>
	</div>
</div>
<div class="m-content">
	<div class="modal fade" id="m_modal_2" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title" id="exampleModalLongTitle">
					Modal title
					</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">
						&times;
					</span>
					</button>
				</div>
				<div class="modal-body">
					<ul class="nav nav-pills nav-fill" role="tablist">
						<li class="nav-item">
							<a class="nav-link active" data-toggle="tab" href="#m_tabs_5_1">
								Basic Information
							</a>
						</li>
						<li class="nav-item">
							<a class="nav-link" data-toggle="tab" href="#m_tabs_5_2">
								Inspection report
							</a>
						</li>
						<li class="nav-item">
							<a class="nav-link" data-toggle="tab" href="#m_tabs_5_3">
								image information
							</a>
						</li>
					</ul>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary" data-dismiss="modal">
					Close
					</button>
				</div>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
	function view(number) {
		getElementByIdClear();

		var sn = $(number).data('id');
		
		$.ajaxSetup({
			headers: {
				'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			}
		});
		$.ajax({
			type: 'GET',
			url: '/warehouse/' + sn,
			dataType: 'json',
			success: function(data) {
				console.log(data);
			},
			error: function(data) {
				console.log(data);
			}
		});
	}
</script>
@endsection
