@extends('layouts.login')
@section('content')
<div class="m-grid m-grid--hor m-grid--root m-page">
    <div class="m-grid__item m-grid__item--fluid m-grid m-grid--hor m-login m-login--2 m-login-2--skin-1" id="" style="background-image: url(assets/app/media/img//bg/bg-1.jpg);">
        <div class="m-grid__item m-grid__item--fluid m-login__wrapper">
            <div class="m-login__container">
                <div class="m-login__logo">
                    <a href="#">
                        <img src="{{ asset('assets/app/media/img//logos/logo-1.png') }}">
                    </a>
                </div>
                <div class="m-login__signup">
                    <div class="m-login__head">
                        <h3 class="m-login__title">
                        Sign Up
                        </h3>
                        <div class="m-login__desc">
                            Enter your details to create your account:
                        </div>
                    </div>
                    <form class="m-login__form m-form" method="POST" action="{{ route('register') }}">
                        @csrf
                        <div class="form-group m-form__group">
                            <input class="form-control m-input" type="text" placeholder="Name" name="name" value="{{ old('name') }}" required autofocus>
                            @error('name')
                                <div id="email-error" class="form-control-feedback" style="color: #f4516c; padding-left: 1.6rem;">{{ $message }}</div>
                            @enderror
                        </div>
                        <div class="form-group m-form__group">
                            <input class="form-control m-input" type="text" placeholder="Email" name="email" autocomplete="off" value="{{ old('email') }}" required autofocus>
                            @error('email')
                                <div id="email-error" class="form-control-feedback" style="color: #f4516c; padding-left: 1.6rem;">{{ $message }}</div>
                            @enderror
                        </div>
                        <div class="form-group m-form__group">
                            <input class="form-control m-input" type="password" placeholder="Password" name="password" required autofocus>
                            @error('password')
                                <div id="email-error" class="form-control-feedback" style="color: #f4516c; padding-left: 1.6rem;">{{ $message }}</div>
                            @enderror
                        </div>
                        <div class="form-group m-form__group">
                            <input class="form-control m-input m-login__form-input--last" type="password" placeholder="Confirm Password" name="password_confirmation" required autofocus>
                        </div>
                        <div class="row form-group m-form__group m-login__form-sub">
                            <div class="col m--align-left">
                                <label class="m-checkbox m-checkbox--light">
                                    <input type="checkbox" name="agree" required autofocus>
                                    I Agree the
                                    <a href="#" class="m-link m-link--focus">
                                        terms and conditions
                                    </a>
                                    .
                                    <span></span>
                                </label>
                                <span class="m-form__help"></span>
                            </div>
                        </div>
                        <div class="m-login__form-action">
                            <button id="" class="btn m-btn m-btn--pill m-btn--custom m-btn--air m-login__btn m-login__btn--primary">
                            Sign Up
                            </button>
                            &nbsp;&nbsp;
                            <button id="" class="btn m-btn m-btn--pill m-btn--custom m-btn--air m-login__btn">
                            Cancel
                            </button>
                        </div>
                    </form>
                </div>
                <div class="m-login__account">
                    <span class="m-login__account-msg">
                        have an account
                    </span>
                    &nbsp;&nbsp;
                    <a href="{{ route('login') }}" id="" class="m-link m-link--light m-login__account-link">
                        Sign In
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection