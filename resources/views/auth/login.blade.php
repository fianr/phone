@extends('layouts.login')
@section('content')
<div class="m-grid m-grid--hor m-grid--root m-page">
	<div class="m-grid__item m-grid__item--fluid m-grid m-grid--hor m-login m-login--signin m-login--2 m-login-2--skin-1" id="m_login" style="background-image: url(assets/app/media/img//bg/bg-1.jpg);">
		<div class="m-grid__item m-grid__item--fluid m-login__wrapper">
			<div class="m-login__container">
				<div class="m-login__logo">
					<a href="#">
						<img src="{{ asset('assets/app/media/img//logos/logo-1.png') }}">
					</a>
				</div>
				<div class="m-login__signin">
					<div class="m-login__head">
						<h3 class="m-login__title">
						Sign In
						</h3>
					</div>
					<form class="m-login__form m-form" method="POST" action="{{ route('login') }}">
						@csrf
						<div class="form-group m-form__group">
							<input class="form-control m-input" type="email" placeholder="Email" name="email" autocomplete="off" value="{{ old('email') }}" required autofocus>
							@error('email')
							<div id="email-error" class="form-control-feedback" style="color: #f4516c; padding-left: 1.6rem;">{{ $message }}</div>
							@enderror
						</div>
						<div class="form-group m-form__group">
							<input class="form-control m-input m-login__form-input--last" type="password" placeholder="Password" name="password" required autofocus>
							@error('password')
							<div id="email-error" class="form-control-feedback" style="color: #f4516c; padding-left: 1.6rem;">{{ $message }}</div>
							@enderror
						</div>
						<div class="row m-login__form-sub">
							<div class="col m--align-left m-login__form-left">
								<label class="m-checkbox  m-checkbox--light">
									<input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}>
									Remember me
									<span></span>
								</label>
							</div>
							@if (Route::has('password.request'))
							<div class="col m--align-right m-login__form-right">
								<a href="{{ route('password.request') }}" id="" class="m-link">
									Forget Password ?
								</a>
							</div>
							@endif
						</div>
						<div class="m-login__form-action">
							<button id="" class="btn btn-focus m-btn m-btn--pill m-btn--custom m-btn--air  m-login__btn m-login__btn--primary">
							Sign In
							</button>
						</div>
					</form>
				</div>
				<div class="m-login__account">
					<span class="m-login__account-msg">
						Don't have an account yet ?
					</span>
					&nbsp;&nbsp;
					<a href="{{ route('register') }}" id="" class="m-link m-link--light m-login__account-link">
						Sign Up
					</a>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection