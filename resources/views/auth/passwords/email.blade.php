@extends('layouts.login')
@section('content')
<div class="m-grid m-grid--hor m-grid--root m-page">
    <div class="m-grid__item m-grid__item--fluid m-grid m-grid--hor m-login m-login--2 m-login-2--skin-1" id="m_login" style="background-image: url({{ asset('assets/app/media/img//bg/bg-1.jpg') }});">
        <div class="m-grid__item m-grid__item--fluid m-login__wrapper">
            <div class="m-login__container">
                <div class="m-login__logo">
                    <a href="#">
                        <img src="{{ asset('assets/app/media/img//logos/logo-1.png') }}">
                    </a>
                </div>
                <div class="m-login__forget-password">
                    <div class="m-login__head">
                        <h3 class="m-login__title">
                        Forgotten Password ?
                        </h3>
                        <div class="m-login__desc">
                            Enter your email to reset your password:
                        </div>
                    </div>
                    @if (session('status'))
                    <div class="m-login__head">
                        <div class="m-login__desc">
                            {{ session('status') }}
                        </div>
                    </div>
                    @endif
                    <form class="m-login__form m-form" method="POST" action="{{ route('password.email') }}">
                        @csrf
                        <div class="form-group m-form__group">
                            <input class="form-control m-input" type="text" placeholder="Email" name="email" id="m_email" autocomplete="off" value="{{ old('email') }}" required autofocus>
                            @error('email')
                            <div id="email-error" class="form-control-feedback" style="color: #f4516c; padding-left: 1.6rem;">{{ $message }}</div>
                            @enderror
                        </div>
                        <div class="m-login__form-action">
                            <button id="" class="btn m-btn m-btn--pill m-btn--custom m-btn--air m-login__btn m-login__btn--primary">
                            Request
                            </button>
                            &nbsp;&nbsp;
                            <button id="" class="btn m-btn m-btn--pill m-btn--custom m-btn--air m-login__btn">
                            Cancel
                            </button>
                        </div>
                    </form>
                </div>
                <div class="m-login__account">
                    <a href="{{ route('login') }}" id="" class="m-link m-link--light m-login__account-link">
                        Sign In
                    </a>
                    &nbsp;&nbsp;
                    <span class="m-login__account-msg">
                        Or
                    </span>
                    &nbsp;&nbsp;
                    <a href="{{ route('register') }}" id="" class="m-link m-link--light m-login__account-link">
                        Sign Up
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection