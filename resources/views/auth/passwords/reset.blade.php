@extends('layouts.login')
@section('content')
<div class="m-grid m-grid--hor m-grid--root m-page">
    <div class="m-grid__item m-grid__item--fluid m-grid m-grid--hor m-login m-login--2 m-login-2--skin-1" id="" style="background-image: url(assets/app/media/img//bg/bg-1.jpg);">
        <div class="m-grid__item m-grid__item--fluid m-login__wrapper">
            <div class="m-login__container">
                <div class="m-login__logo">
                    <a href="#">
                        <img src="{{ asset('assets/app/media/img//logos/logo-1.png') }}">
                    </a>
                </div>
                <div class="m-login__signup">
                    <div class="m-login__head">
                        <h3 class="m-login__title">
                        Reset Password
                        </h3>
                        <div class="m-login__desc">
                            Enter your password account:
                        </div>
                    </div>
                    <form class="m-login__form m-form" method="POST" action="{{ route('password.update') }}">
                        @csrf
                        <div class="form-group m-form__group">
                            <input class="form-control m-input" type="text" placeholder="Email" name="email" value="{{ old('email') }}" required autofocus>
                            @error('email')
                                <div id="email-error" class="form-control-feedback" style="color: #f4516c; padding-left: 1.6rem;">{{ $message }}</div>
                            @enderror
                        </div>
                        <div class="form-group m-form__group">
                            <input class="form-control m-input" type="password" placeholder="Password" name="password" required autofocus>
                            @error('password')
                                <div id="email-error" class="form-control-feedback" style="color: #f4516c; padding-left: 1.6rem;">{{ $message }}</div>
                            @enderror
                        </div>
                        <div class="form-group m-form__group">
                            <input class="form-control m-input m-login__form-input--last" type="password" placeholder="Confirm Password" name="password_confirmation" required autofocus>
                        </div>
                        <div class="m-login__form-action">
                            <button id="" class="btn m-btn m-btn--pill m-btn--custom m-btn--air m-login__btn m-login__btn--primary">
                            Reset Password
                            </button>
                            &nbsp;&nbsp;
                            <button id="" class="btn m-btn m-btn--pill m-btn--custom m-btn--air m-login__btn">
                            Cancel
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection