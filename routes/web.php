<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect()->route('login');
});

Auth::routes(['verify' => true]);

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/home/chart', 'HomeController@chart')->name('home.chart');

Route::get('/order/similar', 'OrderController@similar')->name('order.similar');
Route::post('/order/cart', 'OrderController@cart')->name('order.cart');
Route::get('/order/show_cart', 'OrderController@showCart')->name('order.show_cart');

Route::resources([
    'order' => 'OrderController',
    'warehouse' => 'WarehouseController',
]);