var noUiSliderDemos = {
	init: function() {
		! function() {
			var e = document.getElementById("m_nouislider_3");
			if (e != null) {
				noUiSlider.create(e, {
					start: [0, 0],
					connect: !0,
					direction: "ltr",
					tooltips: [false, false],
					format: wNumb({
						decimals: 3,
						thousand: ".",
					}),
					range: {
						min: [0],
						max: 5000000
					}
				});
				var n = document.getElementById("m_nouislider_3.1_input"),
					t = [document.getElementById("m_nouislider_3_input"), n];
				e.noUiSlider.on("update", function(e, n) {
					t[n].value = e[n];		
					if (e[n] != 0) {
						submit.disabled = false;
					} else {
						submit.disabled = true;
					}
				});
			}
		}()
	}
};
jQuery(document).ready(function() {
	noUiSliderDemos.init()
});
