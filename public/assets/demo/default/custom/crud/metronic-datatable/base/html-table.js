var DatatableHtmlTableDemo = {
    init: function() {
        var e;
        e = $(".m-datatable").mDatatable({
            order: [ 0, 'asc' ],
            processing: false,
            rows: {
                autoHide: true,
            },
            toolbar: {
                items: {
                    info: false,
                }
            },
            scroller: {
                loadingIndicator: false,
            },
            columns: [
                {
                    field: "no",
                    width: 20,
                    ordering: false,
                },
                {
                    field: "sn",
                    width: 150,
                }
            ]
        })
    }
};
jQuery(document).ready(function() {
    DatatableHtmlTableDemo.init()
});