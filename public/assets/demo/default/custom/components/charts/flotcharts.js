var FlotchartsDemo = function() {
    var t = function() {
        $.get('/home/chart', function(data, status) {
            var preorder = data['preorder'],
                order = data['order'],
                qcBill = data['qcBill'];
            
            
            $.plot($("#m_flotcharts_2"), [{
                data: preorder,
                label: "Preorder",
                lines: {
                    lineWidth: 1
                },
                shadowSize: 0
            }, {
                data: order,
                label: "Order",
                lines: {
                    lineWidth: 1
                },
                shadowSize: 0
            }, {
                data: qcBill,
                label: "Warehouse",
                lines: {
                    lineWidth: 1
                },
                shadowSize: 0
            }], {
                series: {
                    lines: {
                        show: !0,
                        lineWidth: 2,
                        fill: !0,
                        fillColor: {
                            colors: [{
                                opacity: .05
                            }, {
                                opacity: .01
                            }]
                        }
                    },
                    points: {
                        show: !0,
                        radius: 3,
                        lineWidth: 1
                    },
                    shadowSize: 2
                },
                grid: {
                    hoverable: !0,
                    clickable: !0,
                    tickColor: "#eee",
                    borderColor: "#eee",
                    borderWidth: 1
                },
                colors: ["#d12610", "#37b7f3", "#52e136"],
                xaxis: {
                    ticks: 11,
                    tickDecimals: 0,
                    tickColor: "#eee"
                },
                yaxis: {
                    ticks: 11,
                    tickDecimals: 0,
                    tickColor: "#eee"
                }
            });
        });
    };
    return {
        init: function() {
            t()
        }
    }
}();
jQuery(document).ready(function() {
    if ($("#m_flotcharts_2").length > 0) {
        FlotchartsDemo.init();
    }
});