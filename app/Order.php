<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $connection = 'mysql';

    protected $table = 'order';

    protected $primaryKey = 'order_id';

    const Dibatalkan = 0;
    const Menunggu_Konfirmasi = 1;
    const Pesanan_Diproses = 2;
    const Pesanan_Selesai = 3;

    const NotActive = 0;
    const Active = 1;

    protected $fillable = [
        'user_id',
        'd_order_id',
        'status',
        'active',
    ];
}
