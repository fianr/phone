<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Redis;
use Illuminate\Http\Request;
use App\Sku\Brand;
use App\Sku\ItemBase;
use App\Sku\Grade;
use App\Sku\Preorder;
use App\Sku\Order;
use App\Sku\QcBill;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware(['auth', 'verified']);
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('home');
    }

    public function chart()
    {
        return self::chartValue();
    }

    protected function chartValue()
    {
        $date = date('j');
        $min = $date - 10;

        for ($a = $min; $a <= $date; $a++) {

            $start = strtotime(date('Y-m-'.$a).' 00:00:00');
            $end =  strtotime(date('Y-m-'.$a).' 23:59:59');
            
            $preorder[] = [$a, self::count($start, $end, 'preorder')];
            $order[] = [$a, self::count($start, $end, 'order')];
            $qcBill[] = [$a, self::count($start, $end, 'qcBill')];
            self::count($start, $end, 'default');
        }

        return [
            'preorder' => $preorder,
            'order' => $order,
            'qcBill' => $qcBill,
        ];
    }

    protected function count($start, $end, $type)
    {
        $key = 'home:chart:'.$start.':'.$end.':'.$type;
        switch ($type) {
            case 'preorder':
                if(Redis::exists($key)){
                    return Redis::get($key);
                }
                $preorder = Preorder::where('fbill_time', '>', $start)
                    ->where('fbill_time', '<', $end)
                    ->count();
                Redis::set($key, $preorder, 'EX', 3600*6);
                return $preorder;
                break;

            case 'order':
                if(Redis::exists($key)){
                    return Redis::get($key);
                }
                $order = Order::where('fbill_time', '>', $start)
                    ->where('fbill_time', '<', $end)
                    ->count();
                Redis::set($key, $order, 'EX', 3600*6);
                return $order;
                break;

            case 'qcBill':
                if(Redis::exists($key)){
                    return Redis::get($key);
                }
                $qcBill = QcBill::where('fbillTime', '>', $start)
                    ->where('fbillTime', '<', $end)
                    ->count();
                Redis::set($key, $qcBill, 'EX', 3600*6);
                return $qcBill;
                break;
            
            default:
                Redis::del($key);
                break;
        }
    }

    protected function redis($type)
    {
        switch ($type) {
            case 'home:index:brand':
                if(Redis::exists($type)){
                    return Redis::get($type);
                }
                $brand = Brand::where('fstatus', 1)->count();
                Redis::set($type, $brand, 'EX', 3600*6);
                return $brand;
                break;
            case 'home:index:itemBase':
                if(Redis::exists($type)){
                    return Redis::get($type);
                }
                $itemBase = ItemBase::where('fstatus', 1)->count();
                Redis::set($type, $itemBase, 'EX', 3600*6);
                return $itemBase;
                break;
            case 'home:index:grade':
                if(Redis::exists($type)){
                    return Redis::get($type);
                }
                $grade = Grade::where('fstatus', 1)->count();
                Redis::set($type, $grade, 'EX', 3600*6);
                return $grade;
                break;
            case 'home:index:preorder':
                if(Redis::exists($type)){
                    return Redis::get($type);
                }
                $preorder = Preorder::where('fstatus', 1)->count();
                Redis::set($type, $preorder, 'EX', 3600*6);
                return $preorder;
                break;
            case 'home:index:order':
                if(Redis::exists($type)){
                    return Redis::get($type);
                }
                $order = Order::where('fstatus', 96)->count();
                Redis::set($type, $order, 'EX', 3600*6);
                return $order;
                break;
            case 'home:index:qcBill':
                if(Redis::exists($type)){
                    return Redis::get($type);
                }
                $qcBill = QcBill::where('fstatus', 10)->count();
                Redis::set($type, $qcBill, 'EX', 3600*6);
                return $qcBill;
                break;
            default:
                Redis::del($type);
                break;
        }
    }
}
