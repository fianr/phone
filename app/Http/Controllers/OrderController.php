<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Redis;
use Illuminate\Http\Request;
use App\Sku\Order;
use App\Sku\PreorderProperty;
use App\Sku\QcBill;
use App\Sku\QcBillStep;
use App\Sku\QcBillAttribute;
use App\Sku\QcTemAttribute;
use App\Sku\QcBillAttributeValue;
use App\Sku\Brand;
use App\Sku\ItemBase;
use App\Sku\Grade;
use App\Order as AppOrder;
use Illuminate\Support\Facades\Auth;

class OrderController extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth', 'verified']);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    // SELECT * FROM `d_order` WHERE d_order.fstatus = 96 AND NOT EXISTS (SELECT * FROM joy_wms.w_sale_item WHERE joy_wms.w_sale_item.fitemSn = d_order.fsn) LIMIT 100

    // working but slow
    // $orders = Order::where('d_order.fstatus', 96)->whereDoesntHave('saleItem', function($query) {
    //     $query->select('*')->from('joy_wms.w_sale_item')->whereRaw('joy_wms.w_sale_item.fitemSn = d_order.fsn');
    // })->count();

    // working but slow
    // $orders = Order::where('d_order.fstatus', 96)->whereDoesntHave('saleItem', function($query) {
    //     $query->from('joy_wms.w_sale_item');
    // })->count();

    public function index(Request $request)
    {
        $brand = self::redis('order:index:brand:status:1:pluck');
        $model = self::redis('order:index:model:status:1:pluck');
        $grade = self::redis('order:index:grade:status:1:pluck');
        $orders = Order::where('d_order.fstatus', 96)->when($request, function ($query) use ($request) {
                $this->check($request);
                $this->search($query, $request);
        })->orderBy('d_order.fid', 'desc')->paginate(10);
        $orders->appends($request->all());
        return view('order.index',compact('orders', 'brand', 'model', 'grade'))->with('i');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
 
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function show($sn)
    {
        $order = $this->order($sn);
        $reportProperty = $this->reportProperty($sn);
        return response()->json([
            'order' => $order,
            'reportProperty' => $reportProperty,
        ], 200);
    }

    public function showCart()
    {
        $appOrder = AppOrder::where('user_id', Auth::user()->user_id)->get();
        foreach ($appOrder as $value) {
            $result[] = $this->findAppOrder($value->d_order_id);
        }
        return response()->json([
            'data' => $result,
        ], 200);
    }

    protected function findAppOrder($id)
    {
        $order = $this->findOrder($id);
        return [
            'sn' => $order->fsn,
            'brand_name' => $order->preorder->itembase->brand->fname,
            'model_name' => $order->fproduct_name,
            'sku_name' => $order->preorder->fsku_name,
            'grade_name' => $order->qcBill ? $order->qcBill->fqcGradeName : $order->preorder->grade->fname,
            'imei' => $order->preorder->fimei,
            'price' => $order->fdeal_amount
        ];
    }

    protected function findOrder($id)
    {
        return Order::find($id);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function cart(Request $request)
    {
        $request->validate([
            'id' => 'required|unique:mysql.order,d_order_id|integer',
            'sn' => 'required|unique:joy_wms.w_sale_item,fitemSn|integer',
        ]);
        
        $appOrder = new AppOrder();
        $appOrder->user_id = Auth::user()->user_id;
        $appOrder->d_order_id = $request->id;
        $appOrder->status = AppOrder::Menunggu_Konfirmasi;
        $appOrder->active = AppOrder::Active;
        $appOrder->save();

        return response()->json([
            'data' => $appOrder,
        ], 200);
    }

    public function similar(Request $request)
    {
        $brand = self::redis('order:index:brand:status:1:pluck');
        $model = self::redis('order:index:model:status:1:pluck');
        $grade = self::redis('order:index:grade:status:1:pluck');
        $orders = Order::when($request, function ($query) use ($request) {
            $this->similarSearch($query, $request);
        })->orderBy('d_order.fid', 'desc')->paginate(10);
        $orders->appends($request->all());
        return view('order.index',compact('orders', 'brand', 'model', 'grade'))->with('i');
    }

    protected function similarSearch($query, $request)
    {
        if (isset($request->order_id)) {
            $query->whereNotIn('d_order.fid', [$request->order_id]);
        }
        if (isset($request->status_id)) {
            $query->where('d_order.fstatus', $request->status_id);
        }
        if (isset($request->brand_id)) {
            $query->whereHas('preorder', function($preorder) use ($request) {
                $preorder->whereHas('itembase', function($itembase) use ($request) {
                    $itembase->whereHas('brand', function($brand) use ($request) {
                        $brand->where('fid', $request->brand_id);
                    });
                });
            });
        }
        if (isset($request->model_id)) {
            $query->whereHas('preorder', function($preorder) use ($request) {
                $preorder->where('ftype_id', $request->model_id);
            });
        }
        if (isset($request->sku_id) && $query->count() >= 25) {
            $query->whereHas('preorder', function($preorder) use ($request) {
                $preorder->where('fitemsku_id', $request->sku_id);
            });
        }
        if (isset($request->grade_id) && $query->count() >= 25) {
            $query->whereHas('preorder', function($preorder) use ($request) {
                $preorder->where('fgrade_id', $request->grade_id);
            });
        }

        return $query;
    }

    protected function order($sn)
    {
        $order = Order::with('orderFile')->where('fsn', $sn)->with('preorder')->first();
        return [
            'serial_number' => $order->fsn,
            'brand_name' => $order->preorder->itemBase->brand->fname,
            'model_name' => $order->fproduct_name,
            'grade_name' => $order->qcBill->fqcGradeName,
            'price' => number_format($order->fdeal_amount),
            'sku_name' => $order->preorder->fsku_name,
            'model_picture' => $this->orderFile($order, 2),
            'id_card_picture' => $this->orderFile($order, 1),
        ];
    }

    protected function orderFile($order, $type)
    {
        foreach($order->orderFile->where('ftype', $type) as $model_picture)
        {
            $data[] = [
                'url' => 'https://ossapi.joyexchange.id/'.$model_picture->fpath,
            ];
        }

        return $data;
    }

    protected function reportProperty($sn)
    {
        $Order = Order::where('fsn', $sn)->first();
        $PreorderProperty = PreorderProperty::where('fpreorder_id', $Order->preorder->fid)->orderBy('fauxproduct_type')->get();
        $QcBill = QcBill::where('fitemId', $Order->fid)->orderBy('fid', 'desc')->first();
        $QcBillStep = QcBillStep::where('fqcBillId', $QcBill->fid)->pluck('fid');
        $QcBillAttribute = QcBillAttribute::whereIn('fqcBillStepId', $QcBillStep)->get();
        foreach ($QcBill->QcBillImg as $key => $value) {
            $image[$value->fqcAttributeId][] = [
                'image_id' => $value->fqcAttributeId,
                'image_url' => 'https://ossapi.joyexchange.id/'.$value->fpath,
            ];
        }

        foreach ($QcBillAttribute as $key => $value) {
            $return[$value->fqcAttributeId] = [
                'property_id_qc' => $value->fqcAttributeId,
                'property_name_qc' => $this->QcTemAttribute($value->fqcAttributeId),
                'property_value_qc' => $this->QcBillAttributeValue($value->fid),
                'property_diff' => $value->fdiff,
            ];
        }

        foreach ($PreorderProperty as $key => $value) {
            $data[] = [
                'property_id_store' => $value->fauxproduct_id,
                'property_name_store' => $value->fauxproduct_name,
                'property_value_store' => $value->fauxitem_name,
                'property_id_qc' => $return[$value->fauxproduct_id]['property_id_qc'],
                'property_name_qc' => $return[$value->fauxproduct_id]['property_name_qc'],
                'property_value_qc' => $return[$value->fauxproduct_id]['property_value_qc'],
                'property_image_qc' => isset($image[$value->fauxproduct_id]) ? $image[$value->fauxproduct_id] : NULL,
                'property_diff' => $return[$value->fauxproduct_id]['property_diff'],
            ];
        }

        return $data;

    }

    protected function QcTemAttribute($fqcAttributeId)
    {
        $QcTemAttribute = QcTemAttribute::where('fattributeId', $fqcAttributeId)->first();
        return $QcTemAttribute->fattributeName;
    }

    protected function QcBillAttributeValue($fid)
    {
        $QcBillAttributeValue = QcBillAttributeValue::find($fid);
        return $QcBillAttributeValue->fomsName;
    }

    protected function check($request)
    {
        if ($request->sn == null) {
            $request->request->remove('sn');
        }
        if ($request->brand_id == "off") {
            $request->request->remove('brand_id');
        }
        if ($request->model_id == "off") {
            $request->request->remove('model_id');
        }
        if ($request->grade_id == "off") {
            $request->request->remove('grade_id');
        }
        if ($request->sku_id == null) {
            $request->request->remove('sku_id');
        }
        if ($request->start_price == 0 && $request->end_price == 0) {
            $request->request->remove('start_price');
            $request->request->remove('end_price');
        }

        return true;
    }

    protected function search($query, $request)
    {
        if (isset($request->sn)) {
            $query->where('fsn', $request->sn);
        }
        if (isset($request->brand_id)) {
            $query->whereHas('preorder', function($preorder) use ($request) {
                $preorder->whereHas('itembase', function($itembase) use ($request) {
                    $itembase->whereHas('brand', function($brand) use ($request) {
                        $brand->where('fid', $request->brand_id);
                    });
                });
            });
        }
        if (isset($request->grade_id)) {
            $query->whereHas('preorder', function($preorder) use ($request) {
                $preorder->where('fgrade_id', $request->grade_id);
            });
        }
        if (isset($request->model_id)) {
            $query->whereHas('preorder', function($preorder) use ($request) {
                $preorder->where('ftype_id', $request->model_id);
            });
        }
        if (isset($request->sku_id)) {
            $query->whereHas('preorder', function($preorder) use ($request) {
                $preorder->where('fsku_name', 'like', "%{$request->sku_id}%");
            });
        }
        if (isset($request->start_price) && isset($request->end_price)) {
            $query->whereBetween('d_order.fdeal_amount', [str_replace('.', '', $request->start_price), str_replace('.', '', $request->end_price)]);
        }

        if (isset($request->diff_price)) {
            $query->where('fdiff_amount', '>',$request->diff_price);
        }

        return $query;
    }

    protected function redis($type)
    {
        switch ($type) {
            case 'order:index:brand:status:1:pluck':
                if(Redis::exists($type)){
                    return json_decode(Redis::get($type));
                }

                $brand = Brand::where('fstatus', 1)->orderBy('fname', 'asc')->pluck('fname', 'fid');
                return Redis::set($type, json_encode($brand), 'EX', 3600*12);
                break;

            case 'order:index:model:status:1:pluck':
                if(Redis::exists($type)){
                    return json_decode(Redis::get($type));
                }

                $model = ItemBase::where('fstatus', 1)->orderBy('fname', 'asc')->pluck('fname', 'fid');
                return Redis::set($type, json_encode($model), 'EX', 3600*12);                
                break;

            case 'order:index:grade:status:1:pluck':
                if(Redis::exists($type)){
                    return json_decode(Redis::get($type));
                }

                $grade = Grade::where('fstatus', 1)->orderBy('fname', 'asc')->pluck('fname', 'fid');
                return Redis::set($type, json_encode($grade), 'EX', 3600*12);
                break;
           
            default:
                Redis::del($type);
                break;
        }
    }

    protected function showRedis($type, $sn)
    {
        switch ($type) {
            case 'show:order':
                if(Redis::exists($type.':'.$sn)){
                    return json_decode(Redis::get($type.':'.$sn));
                }

                $order = $this->order($sn);
                return Redis::set($type.':'.$sn, json_encode($order), 'EX', 3600*12);
                break;
            
            case 'show:preorder':
                if(Redis::exists($type.':'.$sn)){
                    return json_decode(Redis::get($type.':'.$sn));
                }

                $preorder = $this->reportProperty($sn);
                return Redis::set($type.':'.$sn, json_encode($preorder), 'EX', 3600*12);
                break;
            default:
                
                Redis::del($type.':'.$sn);
                break;
        }
        
         
    }
}
