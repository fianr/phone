<?php

namespace App\Http\Controllers;

use App\Sku\QcBill;
use Illuminate\Http\Request;

class WarehouseController extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth', 'verified']);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $qcBill = QcBill::where('fstatus', 10)
            ->orderBy('w_qc_bill.fid', 'desc')->paginate(10);
        return view('warehouse.index',compact('qcBill'))->with('i');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($sn)
    {
        $qcBill = $this->qcBill($sn);
        dd($qcBill);
        return response()->json([
            'order' => 'oke',
        ], 200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    protected function qcBill($sn)
    {
        $qcBill = QcBill::where('fitemSn', $sn)->first();
        return [
            'serial_number' => $qcBill->fitemSn,
            'brand_name' => $qcBill->ItemBase->brand->fname,
            'model_name' => $qcBill->fitembaseName,
            'grade_name' => $qcBill->fqcGradeName,
            'sku_name' => $qcBill->fitemSkuName,
            'price' => $qcBill->fqcAmount,
        ];
    }
}
