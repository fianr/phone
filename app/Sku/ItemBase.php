<?php

namespace App\Sku;

use Illuminate\Database\Eloquent\Model;

class ItemBase extends Model
{
    protected $connection = 'sku';

    protected $table = 'd_itembase';

    protected $primaryKey = 'fid';

    protected $fillable = [
    ];

    public function brand()
    {
        return $this->hasOne(Brand::class, 'fid', 'fbrand_id');
    }
}
