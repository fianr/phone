<?php

namespace App\Sku;

use Illuminate\Database\Eloquent\Model;

class Grade extends Model
{
    protected $connection = 'sku';

    protected $table = 'd_grade';

    protected $primaryKey = 'fid';

    protected $fillable = [];
    
}
