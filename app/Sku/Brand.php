<?php

namespace App\Sku;

use Illuminate\Database\Eloquent\Model;

class Brand extends Model
{
    protected $connection = 'sku';

    protected $table = 'd_brand';

    protected $primaryKey = 'fid';

    protected $fillable = [
    ];
}
