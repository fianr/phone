<?php

namespace App\Sku;

use Illuminate\Database\Eloquent\Model;

class Address extends Model
{
	protected $connection = 'sku';

	protected $table = 'd_address';

	protected $primaryKey = 'fid';

	protected $fillable = [
	    'name',
	    'email',
	    'password',
	];

	protected $hidden = [
	    'password',
	    'remember_token',
	];

	protected $casts = [
	    'email_verified_at' => 'datetime',
	];
}
