<?php

namespace App\Sku;

use App\JoyWms\SaleItem;
use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $connection = 'sku';

    protected $table = 'd_order';

    protected $primaryKey = 'fid';

    const INVALID = 0;
    const INSPECTION = 1;
    const SUCCESS = 10;
    const DIFF = 20;

    protected $fillable = [
    ];

    public function preorder()
    {
        return $this->hasOne(Preorder::class, 'fid', 'fpreorder_id');
    }

    public function orderFile()
    {
        return $this->hasMany(OrderFile::class, 'forder_id', 'fid');
    }

    public function qcBill()
    {
        return $this->hasOne(QcBill::class, 'fitemId', 'fid')->orderBy('fid', 'desc');
    }

    public function saleItem()
    {
        return $this->hasOne(SaleItem::class, 'fitemSn', 'fsn');
    }

    public $statusName = [
        0 => 'Preorder Pending',
        1 => 'Preorder Success',
        10 => 'Pending',
        11 => 'Unshipped',
        12 => 'Shipped',
        21 => 'Inspection',
        22 => 'Inspection success',
        23 => 'Inspection diff',
        24 => 'Immunity',
        25 => 'UnReturn',
        26 => 'Returning',
        30 => 'Unpayed',
        90 => 'Return',
        91 => 'Cancelled',
        96 => 'Order Success',
    ];

}
