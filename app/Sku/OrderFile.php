<?php

namespace App\Sku;

use Illuminate\Database\Eloquent\Model;

class OrderFile extends Model
{
    protected $connection = 'sku';

    protected $table = 'd_order_file';

    protected $primaryKey = 'fid';

    protected $fillable = [];
}
