<?php

namespace App\Sku;

use Illuminate\Database\Eloquent\Model;

class QcBillImg extends Model
{
    protected $connection = 'joy_wms';

    protected $table = 'w_qc_bill_img';

    protected $primaryKey = 'fid';

    protected $fillable = [];
}
