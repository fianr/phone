<?php

namespace App\Sku;

use Illuminate\Database\Eloquent\Model;

class PreorderProperty extends Model
{
    protected $connection = 'sku';

    protected $table = 'd_preorder_property';

    protected $primaryKey = 'fid';

    protected $fillable = [
    ];
}
