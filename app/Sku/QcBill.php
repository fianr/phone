<?php

namespace App\Sku;

use Illuminate\Database\Eloquent\Model;

class QcBill extends Model
{
    protected $connection = 'joy_wms';

    protected $table = 'w_qc_bill';

    protected $primaryKey = 'fid';

    const INVALID = 0;
    const INSPECTION = 1;
    const SUCCESS = 10;
    const DIFF = 20;

    public $statusName = [
        self::INVALID => 'Invalid',
        self::INSPECTION => 'Inspection',
        self::SUCCESS => 'Success',
        self::DIFF => 'Inspection Diff'

    ];

    protected $fillable = [];

    public function QcBillImg()
    {
        return $this->hasMany(QcBillImg::class, 'fqcBillId', 'fid');
    }

    public function QcBillAttribute()
    {
    	return $this->hasMany(QcBillAttribute::class, 'fid', 'fid');
    }

    public function Grade()
    {
        return $this->hasOne(Grade::class, 'fid', 'fomsGradeId');
    }

    public function ItemBase()
    {
        return $this->hasOne(ItemBase::class, 'fid', 'fitembaseId');
    }

    public function Order()
    {
        return $this->hasOne(Order::class, 'fid', 'fitemId');
    }
}
