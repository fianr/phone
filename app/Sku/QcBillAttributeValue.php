<?php

namespace App\Sku;

use Illuminate\Database\Eloquent\Model;

class QcBillAttributeValue extends Model
{
	protected $connection = 'joy_wms';

    protected $table = 'w_qc_bill_attribute_value';

    protected $primaryKey = 'fqcBillAttributeId';

    protected $fillable = [];
}
