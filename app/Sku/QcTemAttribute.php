<?php

namespace App\Sku;

use Illuminate\Database\Eloquent\Model;

class QcTemAttribute extends Model
{
    protected $connection = 'joy_wms';

    protected $table = 'd_qc_tem_attribute';

    protected $primaryKey = 'fid';

    protected $fillable = [];
}
