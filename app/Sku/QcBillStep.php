<?php

namespace App\Sku;

use Illuminate\Database\Eloquent\Model;

class QcBillStep extends Model
{
    protected $connection = 'joy_wms';

    protected $table = 'w_qc_bill_step';

    protected $primaryKey = 'fid';

    protected $fillable = [];
}
