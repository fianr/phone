<?php

namespace App\Sku;

use Illuminate\Database\Eloquent\Model;

class QcBillAttribute extends Model
{
    protected $connection = 'joy_wms';

    protected $table = 'w_qc_bill_attribute';

    protected $primaryKey = 'fid';

    protected $fillable = [];

    public function QcTemAttribute()
    {
        return $this->hasOne(QcTemAttribute::class, 'fattributeId', 'fqcAttributeId');
    }
}
