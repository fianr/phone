<?php

namespace App\Sku;

use Illuminate\Database\Eloquent\Model;

class Preorder extends Model
{
    protected $connection = 'sku';

    protected $table = 'd_preorder';

    protected $primaryKey = 'fid';

    protected $fillable = [
    ];

    public function grade()
    {
        return $this->hasOne(Grade::class, 'fid', 'fgrade_id');
    }

    public function preorderProperty($id)
    {
        return PreorderProperty::where('fpreorder_id', $id)->get();
    }

    public function itemBase()
    {
        return $this->hasOne(ItemBase::class, 'fid', 'ftype_id');
    }

}
