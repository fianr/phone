<?php

namespace App\JoyWms;

use App\Sku\Order;
use Illuminate\Database\Eloquent\Model;

class SaleItem extends Model
{
    protected $connection = 'joy_wms';

    protected $table = 'w_sale_item';

    protected $primaryKey = 'fid';

    protected $fillable = [
    ];

    public function Order()
    {
        return $this->hasOne(Order::class, 'fsn', 'fitemSn');
    }
}
