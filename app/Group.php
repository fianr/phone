<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Group extends Model
{
    protected $connection = 'mysql';

    protected $table = 'group';

    protected $primaryKey = 'group_id';

    const Admin = 1;
    const User = 2;

    protected $fillable = [
        'name',
        'active',
    ];
}
