<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DetailUser extends Model
{
    protected $connection = 'mysql';

    protected $table = 'detail_user';

    protected $primaryKey = 'detail_user_id';

    protected $fillable = [
        'user_id',
        'group_id',
    ];
}
